package osk.web.model;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.InverseModelListRef;
import org.slim3.datastore.InverseModelRef;
import org.slim3.datastore.Model;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Text;

@Model
public class Profile {

    @Attribute(primaryKey=true)
    private Key key;

    /**
     * 名前
     */
    private String name="名称未設定";

    /**
     * ニックネーム
     */
    private String nickName="名称未設定";

    /**
     * メアド
     */
    private String mailAddress="";

    /*
     *学籍番号
     */
    private String schoolNumber;

    /**
     * 学部
     */
    private String department;

    /**
     *学科
     */
    private String major;


    /**
     * 使える言語
     */
    private String usableLanguage;

    /*
     *好きなアニメ
     */
    private String favoriteAnime;

    /**
     * 好きな数式
     */
    private String favoriteFormula;

    /**
     * 自己紹介
     */
    private Text selfIntroduction;

    /**
     * OBかそうでないか
     */
    private Boolean isOB=false;

    /**
     * 男かそうでないか
     */
    private Boolean isMale=true;

    /**
     * 郵便番号
     */
    private String zipCode;

    /**
     * 住所
     */
    private String address;

    /**
     * GitHubのアドレス
     */
    private String githubURL;

    /**
     * ビットバケットのアドレス
     */
    private String bitBucketURL;

    /**
     * ツイッターのユーザー名
     */
    private String twitterUserName;

    @Attribute(persistent=false)
    private InverseModelRef<Authorization, Profile> authorizationInverseModelRef=new InverseModelRef<Authorization, Profile>(Authorization.class, "profile", this);
    public InverseModelRef<Authorization, Profile> getAuthorizationInverseModelRef() {
        return authorizationInverseModelRef;
    }

    @Attribute(persistent=false)
    private InverseModelListRef<RoleGlue, Profile> roleRef=new InverseModelListRef<RoleGlue, Profile>(RoleGlue.class, "profileRef", this);

    public InverseModelListRef<RoleGlue, Profile> getRoleRef() {
        return roleRef;
    }

    /**
     * 任意のアドレス
     */
    private String url;

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getSchoolNumber() {
        return schoolNumber;
    }

    public void setSchoolNumber(String schoolNumber) {
        this.schoolNumber = schoolNumber;
    }

    public String getUsableLanguage() {
        return usableLanguage;
    }

    public void setUsableLanguage(String usableLanguage) {
        this.usableLanguage = usableLanguage;
    }

    public String getFavoriteAnime() {
        return favoriteAnime;
    }

    public void setFavoriteAnime(String favoriteAnime) {
        this.favoriteAnime = favoriteAnime;
    }

    public String getFavoriteFormula() {
        return favoriteFormula;
    }

    public void setFavoriteFormula(String favoriteFormula) {
        this.favoriteFormula = favoriteFormula;
    }

    public Text getSelfIntroduction() {
        return selfIntroduction;
    }

    public void setSelfIntroduction(Text selfIntroduction) {
        this.selfIntroduction = selfIntroduction;
    }

    public Boolean getIsOB() {
        return isOB;
    }

    public void setIsOB(Boolean isOB) {
        this.isOB = isOB;
    }

    public Boolean getIsMale() {
        return isMale;
    }

    public void setIsMale(Boolean isMale) {
        this.isMale = isMale;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGithubURL() {
        return githubURL;
    }

    public void setGithubURL(String githubURL) {
        this.githubURL = githubURL;
    }

    public String getBitBucketURL() {
        return bitBucketURL;
    }

    public void setBitBucketURL(String bitBucketURL) {
        this.bitBucketURL = bitBucketURL;
    }

    public String getTwitterUserName() {
        return twitterUserName;
    }

    public void setTwitterUserName(String twitterUserName) {
        this.twitterUserName = twitterUserName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

}
