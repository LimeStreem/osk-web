package osk.web.model;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Model;
import org.slim3.datastore.ModelRef;

import com.google.appengine.api.datastore.Key;

/***
 * データストアのモデルクラス
 *
 */
@Model
public class Authorization
{
    /***
     * プライマリーキー
     */
    @Attribute(primaryKey=true)
    private Key key;

    /***
     * ID
     */
    private String id;

    /***
     * パスワード
     */
    @Attribute(cipher=true)
    private String pass;

    /***
     * 管理者かどうか
     */
    private Boolean isAdmin=false;

    /**
     * このユーザーのプロファイル
     */
    private ModelRef<Profile> profile=new ModelRef<Profile>(Profile.class);

    public Key getKey() {
        return key;
    }

    public void setKey(Key key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public ModelRef<Profile> getProfile() {
        return profile;
    }

}
