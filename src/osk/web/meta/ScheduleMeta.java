package osk.web.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-12-21 17:05:19")
/** */
public final class ScheduleMeta extends org.slim3.datastore.ModelMeta<osk.web.model.Schedule> {

    /** */
    public final org.slim3.datastore.UnindexedAttributeMeta<osk.web.model.Schedule, com.google.appengine.api.datastore.Text> content = new org.slim3.datastore.UnindexedAttributeMeta<osk.web.model.Schedule, com.google.appengine.api.datastore.Text>(this, "content", "content", com.google.appengine.api.datastore.Text.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Schedule> date = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Schedule>(this, "date", "date");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Schedule, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Schedule, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Schedule, java.lang.Integer> month = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Schedule, java.lang.Integer>(this, "month", "month", int.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Schedule> roleTag = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Schedule>(this, "roleTag", "roleTag");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Schedule> tag = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Schedule>(this, "tag", "tag");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Schedule> title = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Schedule>(this, "title", "title");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Schedule, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Schedule, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final ScheduleMeta slim3_singleton = new ScheduleMeta();

    /**
     * @return the singleton
     */
    public static ScheduleMeta get() {
       return slim3_singleton;
    }

    /** */
    public ScheduleMeta() {
        super("Schedule", osk.web.model.Schedule.class);
    }

    @Override
    public osk.web.model.Schedule entityToModel(com.google.appengine.api.datastore.Entity entity) {
        osk.web.model.Schedule model = new osk.web.model.Schedule();
        model.setContent((com.google.appengine.api.datastore.Text) entity.getProperty("content"));
        model.setDate((java.lang.String) entity.getProperty("date"));
        model.setKey(entity.getKey());
        model.setMonth(longToPrimitiveInt((java.lang.Long) entity.getProperty("month")));
        model.setRoleTag((java.lang.String) entity.getProperty("roleTag"));
        model.setTag((java.lang.String) entity.getProperty("tag"));
        model.setTitle((java.lang.String) entity.getProperty("title"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        osk.web.model.Schedule m = (osk.web.model.Schedule) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setUnindexedProperty("content", m.getContent());
        entity.setProperty("date", m.getDate());
        entity.setProperty("month", m.getMonth());
        entity.setProperty("roleTag", m.getRoleTag());
        entity.setProperty("tag", m.getTag());
        entity.setProperty("title", m.getTitle());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        osk.web.model.Schedule m = (osk.web.model.Schedule) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        osk.web.model.Schedule m = (osk.web.model.Schedule) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        osk.web.model.Schedule m = (osk.web.model.Schedule) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        osk.web.model.Schedule m = (osk.web.model.Schedule) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        osk.web.model.Schedule m = (osk.web.model.Schedule) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getContent() != null && m.getContent().getValue() != null){
            writer.setNextPropertyName("content");
            encoder0.encode(writer, m.getContent());
        }
        if(m.getDate() != null){
            writer.setNextPropertyName("date");
            encoder0.encode(writer, m.getDate());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        writer.setNextPropertyName("month");
        encoder0.encode(writer, m.getMonth());
        if(m.getRoleTag() != null){
            writer.setNextPropertyName("roleTag");
            encoder0.encode(writer, m.getRoleTag());
        }
        if(m.getTag() != null){
            writer.setNextPropertyName("tag");
            encoder0.encode(writer, m.getTag());
        }
        if(m.getTitle() != null){
            writer.setNextPropertyName("title");
            encoder0.encode(writer, m.getTitle());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected osk.web.model.Schedule jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        osk.web.model.Schedule m = new osk.web.model.Schedule();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("content");
        m.setContent(decoder0.decode(reader, m.getContent()));
        reader = rootReader.newObjectReader("date");
        m.setDate(decoder0.decode(reader, m.getDate()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("month");
        m.setMonth(decoder0.decode(reader, m.getMonth()));
        reader = rootReader.newObjectReader("roleTag");
        m.setRoleTag(decoder0.decode(reader, m.getRoleTag()));
        reader = rootReader.newObjectReader("tag");
        m.setTag(decoder0.decode(reader, m.getTag()));
        reader = rootReader.newObjectReader("title");
        m.setTitle(decoder0.decode(reader, m.getTitle()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}