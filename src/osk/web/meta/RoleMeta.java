package osk.web.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-12-21 17:05:19")
/** */
public final class RoleMeta extends org.slim3.datastore.ModelMeta<osk.web.model.Role> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Role, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Role, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Role> roleName = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Role>(this, "roleName", "roleName");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Role> type = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Role>(this, "type", "type");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Role, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Role, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final RoleMeta slim3_singleton = new RoleMeta();

    /**
     * @return the singleton
     */
    public static RoleMeta get() {
       return slim3_singleton;
    }

    /** */
    public RoleMeta() {
        super("Role", osk.web.model.Role.class);
    }

    @Override
    public osk.web.model.Role entityToModel(com.google.appengine.api.datastore.Entity entity) {
        osk.web.model.Role model = new osk.web.model.Role();
        model.setKey(entity.getKey());
        model.setRoleName((java.lang.String) entity.getProperty("roleName"));
        model.setType((java.lang.String) entity.getProperty("type"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        osk.web.model.Role m = (osk.web.model.Role) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("roleName", m.getRoleName());
        entity.setProperty("type", m.getType());
        entity.setProperty("version", m.getVersion());
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        osk.web.model.Role m = (osk.web.model.Role) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        osk.web.model.Role m = (osk.web.model.Role) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        osk.web.model.Role m = (osk.web.model.Role) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        osk.web.model.Role m = (osk.web.model.Role) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        osk.web.model.Role m = (osk.web.model.Role) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getRoleName() != null){
            writer.setNextPropertyName("roleName");
            encoder0.encode(writer, m.getRoleName());
        }
        if(m.getType() != null){
            writer.setNextPropertyName("type");
            encoder0.encode(writer, m.getType());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected osk.web.model.Role jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        osk.web.model.Role m = new osk.web.model.Role();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("roleName");
        m.setRoleName(decoder0.decode(reader, m.getRoleName()));
        reader = rootReader.newObjectReader("type");
        m.setType(decoder0.decode(reader, m.getType()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}