package osk.web.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-12-21 17:05:19")
/** */
public final class ProfileMeta extends org.slim3.datastore.ModelMeta<osk.web.model.Profile> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> address = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "address", "address");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> bitBucketURL = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "bitBucketURL", "bitBucketURL");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> department = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "department", "department");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> favoriteAnime = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "favoriteAnime", "favoriteAnime");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> favoriteFormula = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "favoriteFormula", "favoriteFormula");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> githubURL = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "githubURL", "githubURL");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Profile, java.lang.Boolean> isMale = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Profile, java.lang.Boolean>(this, "isMale", "isMale", java.lang.Boolean.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Profile, java.lang.Boolean> isOB = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Profile, java.lang.Boolean>(this, "isOB", "isOB", java.lang.Boolean.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Profile, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Profile, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> mailAddress = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "mailAddress", "mailAddress");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> major = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "major", "major");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> name = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "name", "name");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> nickName = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "nickName", "nickName");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> schoolNumber = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "schoolNumber", "schoolNumber");

    /** */
    public final org.slim3.datastore.UnindexedAttributeMeta<osk.web.model.Profile, com.google.appengine.api.datastore.Text> selfIntroduction = new org.slim3.datastore.UnindexedAttributeMeta<osk.web.model.Profile, com.google.appengine.api.datastore.Text>(this, "selfIntroduction", "selfIntroduction", com.google.appengine.api.datastore.Text.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> twitterUserName = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "twitterUserName", "twitterUserName");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> url = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "url", "url");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> usableLanguage = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "usableLanguage", "usableLanguage");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile> zipCode = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Profile>(this, "zipCode", "zipCode");

    private static final ProfileMeta slim3_singleton = new ProfileMeta();

    /**
     * @return the singleton
     */
    public static ProfileMeta get() {
       return slim3_singleton;
    }

    /** */
    public ProfileMeta() {
        super("Profile", osk.web.model.Profile.class);
    }

    @Override
    public osk.web.model.Profile entityToModel(com.google.appengine.api.datastore.Entity entity) {
        osk.web.model.Profile model = new osk.web.model.Profile();
        model.setAddress((java.lang.String) entity.getProperty("address"));
        model.setBitBucketURL((java.lang.String) entity.getProperty("bitBucketURL"));
        model.setDepartment((java.lang.String) entity.getProperty("department"));
        model.setFavoriteAnime((java.lang.String) entity.getProperty("favoriteAnime"));
        model.setFavoriteFormula((java.lang.String) entity.getProperty("favoriteFormula"));
        model.setGithubURL((java.lang.String) entity.getProperty("githubURL"));
        model.setIsMale((java.lang.Boolean) entity.getProperty("isMale"));
        model.setIsOB((java.lang.Boolean) entity.getProperty("isOB"));
        model.setKey(entity.getKey());
        model.setMailAddress((java.lang.String) entity.getProperty("mailAddress"));
        model.setMajor((java.lang.String) entity.getProperty("major"));
        model.setName((java.lang.String) entity.getProperty("name"));
        model.setNickName((java.lang.String) entity.getProperty("nickName"));
        model.setSchoolNumber((java.lang.String) entity.getProperty("schoolNumber"));
        model.setSelfIntroduction((com.google.appengine.api.datastore.Text) entity.getProperty("selfIntroduction"));
        model.setTwitterUserName((java.lang.String) entity.getProperty("twitterUserName"));
        model.setUrl((java.lang.String) entity.getProperty("url"));
        model.setUsableLanguage((java.lang.String) entity.getProperty("usableLanguage"));
        model.setZipCode((java.lang.String) entity.getProperty("zipCode"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        osk.web.model.Profile m = (osk.web.model.Profile) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("address", m.getAddress());
        entity.setProperty("bitBucketURL", m.getBitBucketURL());
        entity.setProperty("department", m.getDepartment());
        entity.setProperty("favoriteAnime", m.getFavoriteAnime());
        entity.setProperty("favoriteFormula", m.getFavoriteFormula());
        entity.setProperty("githubURL", m.getGithubURL());
        entity.setProperty("isMale", m.getIsMale());
        entity.setProperty("isOB", m.getIsOB());
        entity.setProperty("mailAddress", m.getMailAddress());
        entity.setProperty("major", m.getMajor());
        entity.setProperty("name", m.getName());
        entity.setProperty("nickName", m.getNickName());
        entity.setProperty("schoolNumber", m.getSchoolNumber());
        entity.setUnindexedProperty("selfIntroduction", m.getSelfIntroduction());
        entity.setProperty("twitterUserName", m.getTwitterUserName());
        entity.setProperty("url", m.getUrl());
        entity.setProperty("usableLanguage", m.getUsableLanguage());
        entity.setProperty("zipCode", m.getZipCode());
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        osk.web.model.Profile m = (osk.web.model.Profile) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        osk.web.model.Profile m = (osk.web.model.Profile) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        throw new IllegalStateException("The version property of the model(osk.web.model.Profile) is not defined.");
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        osk.web.model.Profile m = (osk.web.model.Profile) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getAddress() != null){
            writer.setNextPropertyName("address");
            encoder0.encode(writer, m.getAddress());
        }
        if(m.getBitBucketURL() != null){
            writer.setNextPropertyName("bitBucketURL");
            encoder0.encode(writer, m.getBitBucketURL());
        }
        if(m.getDepartment() != null){
            writer.setNextPropertyName("department");
            encoder0.encode(writer, m.getDepartment());
        }
        if(m.getFavoriteAnime() != null){
            writer.setNextPropertyName("favoriteAnime");
            encoder0.encode(writer, m.getFavoriteAnime());
        }
        if(m.getFavoriteFormula() != null){
            writer.setNextPropertyName("favoriteFormula");
            encoder0.encode(writer, m.getFavoriteFormula());
        }
        if(m.getGithubURL() != null){
            writer.setNextPropertyName("githubURL");
            encoder0.encode(writer, m.getGithubURL());
        }
        if(m.getIsMale() != null){
            writer.setNextPropertyName("isMale");
            encoder0.encode(writer, m.getIsMale());
        }
        if(m.getIsOB() != null){
            writer.setNextPropertyName("isOB");
            encoder0.encode(writer, m.getIsOB());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getMailAddress() != null){
            writer.setNextPropertyName("mailAddress");
            encoder0.encode(writer, m.getMailAddress());
        }
        if(m.getMajor() != null){
            writer.setNextPropertyName("major");
            encoder0.encode(writer, m.getMajor());
        }
        if(m.getName() != null){
            writer.setNextPropertyName("name");
            encoder0.encode(writer, m.getName());
        }
        if(m.getNickName() != null){
            writer.setNextPropertyName("nickName");
            encoder0.encode(writer, m.getNickName());
        }
        if(m.getSchoolNumber() != null){
            writer.setNextPropertyName("schoolNumber");
            encoder0.encode(writer, m.getSchoolNumber());
        }
        if(m.getSelfIntroduction() != null && m.getSelfIntroduction().getValue() != null){
            writer.setNextPropertyName("selfIntroduction");
            encoder0.encode(writer, m.getSelfIntroduction());
        }
        if(m.getTwitterUserName() != null){
            writer.setNextPropertyName("twitterUserName");
            encoder0.encode(writer, m.getTwitterUserName());
        }
        if(m.getUrl() != null){
            writer.setNextPropertyName("url");
            encoder0.encode(writer, m.getUrl());
        }
        if(m.getUsableLanguage() != null){
            writer.setNextPropertyName("usableLanguage");
            encoder0.encode(writer, m.getUsableLanguage());
        }
        if(m.getZipCode() != null){
            writer.setNextPropertyName("zipCode");
            encoder0.encode(writer, m.getZipCode());
        }
        writer.endObject();
    }

    @Override
    protected osk.web.model.Profile jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        osk.web.model.Profile m = new osk.web.model.Profile();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("address");
        m.setAddress(decoder0.decode(reader, m.getAddress()));
        reader = rootReader.newObjectReader("bitBucketURL");
        m.setBitBucketURL(decoder0.decode(reader, m.getBitBucketURL()));
        reader = rootReader.newObjectReader("department");
        m.setDepartment(decoder0.decode(reader, m.getDepartment()));
        reader = rootReader.newObjectReader("favoriteAnime");
        m.setFavoriteAnime(decoder0.decode(reader, m.getFavoriteAnime()));
        reader = rootReader.newObjectReader("favoriteFormula");
        m.setFavoriteFormula(decoder0.decode(reader, m.getFavoriteFormula()));
        reader = rootReader.newObjectReader("githubURL");
        m.setGithubURL(decoder0.decode(reader, m.getGithubURL()));
        reader = rootReader.newObjectReader("isMale");
        m.setIsMale(decoder0.decode(reader, m.getIsMale()));
        reader = rootReader.newObjectReader("isOB");
        m.setIsOB(decoder0.decode(reader, m.getIsOB()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("mailAddress");
        m.setMailAddress(decoder0.decode(reader, m.getMailAddress()));
        reader = rootReader.newObjectReader("major");
        m.setMajor(decoder0.decode(reader, m.getMajor()));
        reader = rootReader.newObjectReader("name");
        m.setName(decoder0.decode(reader, m.getName()));
        reader = rootReader.newObjectReader("nickName");
        m.setNickName(decoder0.decode(reader, m.getNickName()));
        reader = rootReader.newObjectReader("schoolNumber");
        m.setSchoolNumber(decoder0.decode(reader, m.getSchoolNumber()));
        reader = rootReader.newObjectReader("selfIntroduction");
        m.setSelfIntroduction(decoder0.decode(reader, m.getSelfIntroduction()));
        reader = rootReader.newObjectReader("twitterUserName");
        m.setTwitterUserName(decoder0.decode(reader, m.getTwitterUserName()));
        reader = rootReader.newObjectReader("url");
        m.setUrl(decoder0.decode(reader, m.getUrl()));
        reader = rootReader.newObjectReader("usableLanguage");
        m.setUsableLanguage(decoder0.decode(reader, m.getUsableLanguage()));
        reader = rootReader.newObjectReader("zipCode");
        m.setZipCode(decoder0.decode(reader, m.getZipCode()));
        return m;
    }
}