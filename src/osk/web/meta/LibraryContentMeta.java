package osk.web.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-12-21 17:05:19")
/** */
public final class LibraryContentMeta extends org.slim3.datastore.ModelMeta<osk.web.model.LibraryContent> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent> autherId = new org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent>(this, "autherId", "autherId");

    /** */
    public final org.slim3.datastore.ModelRefAttributeMeta<osk.web.model.LibraryContent, org.slim3.datastore.ModelRef<osk.web.model.Profile>, osk.web.model.Profile> autherProfile = new org.slim3.datastore.ModelRefAttributeMeta<osk.web.model.LibraryContent, org.slim3.datastore.ModelRef<osk.web.model.Profile>, osk.web.model.Profile>(this, "autherProfile", "autherProfile", org.slim3.datastore.ModelRef.class, osk.web.model.Profile.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent> blobKey = new org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent>(this, "blobKey", "blobKey");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent> category = new org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent>(this, "category", "category");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent> description = new org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent>(this, "description", "description");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.LibraryContent, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.LibraryContent, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent> title = new org.slim3.datastore.StringAttributeMeta<osk.web.model.LibraryContent>(this, "title", "title");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.LibraryContent, java.util.Date> updatedAt = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.LibraryContent, java.util.Date>(this, "updatedAt", "updatedAt", java.util.Date.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.LibraryContent, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.LibraryContent, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final org.slim3.datastore.ModificationDate slim3_updatedAtAttributeListener = new org.slim3.datastore.ModificationDate();

    private static final LibraryContentMeta slim3_singleton = new LibraryContentMeta();

    /**
     * @return the singleton
     */
    public static LibraryContentMeta get() {
       return slim3_singleton;
    }

    /** */
    public LibraryContentMeta() {
        super("LibraryContent", osk.web.model.LibraryContent.class);
    }

    @Override
    public osk.web.model.LibraryContent entityToModel(com.google.appengine.api.datastore.Entity entity) {
        osk.web.model.LibraryContent model = new osk.web.model.LibraryContent();
        model.setAutherId((java.lang.String) entity.getProperty("autherId"));
        if (model.getAutherProfile() == null) {
            throw new NullPointerException("The property(autherProfile) is null.");
        }
        model.getAutherProfile().setKey((com.google.appengine.api.datastore.Key) entity.getProperty("autherProfile"));
        model.setBlobKey((java.lang.String) entity.getProperty("blobKey"));
        model.setCategory((java.lang.String) entity.getProperty("category"));
        model.setDescription((java.lang.String) entity.getProperty("description"));
        model.setKey(entity.getKey());
        model.setTitle((java.lang.String) entity.getProperty("title"));
        model.setUpdatedAt((java.util.Date) entity.getProperty("updatedAt"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        osk.web.model.LibraryContent m = (osk.web.model.LibraryContent) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("autherId", m.getAutherId());
        if (m.getAutherProfile() == null) {
            throw new NullPointerException("The property(autherProfile) must not be null.");
        }
        entity.setProperty("autherProfile", m.getAutherProfile().getKey());
        entity.setProperty("blobKey", m.getBlobKey());
        entity.setProperty("category", m.getCategory());
        entity.setProperty("description", m.getDescription());
        entity.setProperty("title", m.getTitle());
        entity.setProperty("updatedAt", m.getUpdatedAt());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        osk.web.model.LibraryContent m = (osk.web.model.LibraryContent) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        osk.web.model.LibraryContent m = (osk.web.model.LibraryContent) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        osk.web.model.LibraryContent m = (osk.web.model.LibraryContent) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
        osk.web.model.LibraryContent m = (osk.web.model.LibraryContent) model;
        if (m.getAutherProfile() == null) {
            throw new NullPointerException("The property(autherProfile) must not be null.");
        }
        m.getAutherProfile().assignKeyIfNecessary(ds);
    }

    @Override
    protected void incrementVersion(Object model) {
        osk.web.model.LibraryContent m = (osk.web.model.LibraryContent) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
        osk.web.model.LibraryContent m = (osk.web.model.LibraryContent) model;
        m.setUpdatedAt(slim3_updatedAtAttributeListener.prePut(m.getUpdatedAt()));
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        osk.web.model.LibraryContent m = (osk.web.model.LibraryContent) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getAutherId() != null){
            writer.setNextPropertyName("autherId");
            encoder0.encode(writer, m.getAutherId());
        }
        if(m.getAutherProfile() != null && m.getAutherProfile().getKey() != null){
            writer.setNextPropertyName("autherProfile");
            encoder0.encode(writer, m.getAutherProfile(), maxDepth, currentDepth);
        }
        if(m.getBlobKey() != null){
            writer.setNextPropertyName("blobKey");
            encoder0.encode(writer, m.getBlobKey());
        }
        if(m.getCategory() != null){
            writer.setNextPropertyName("category");
            encoder0.encode(writer, m.getCategory());
        }
        if(m.getDescription() != null){
            writer.setNextPropertyName("description");
            encoder0.encode(writer, m.getDescription());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getTitle() != null){
            writer.setNextPropertyName("title");
            encoder0.encode(writer, m.getTitle());
        }
        if(m.getUpdatedAt() != null){
            writer.setNextPropertyName("updatedAt");
            encoder0.encode(writer, m.getUpdatedAt());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected osk.web.model.LibraryContent jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        osk.web.model.LibraryContent m = new osk.web.model.LibraryContent();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("autherId");
        m.setAutherId(decoder0.decode(reader, m.getAutherId()));
        reader = rootReader.newObjectReader("autherProfile");
        decoder0.decode(reader, m.getAutherProfile(), maxDepth, currentDepth);
        reader = rootReader.newObjectReader("blobKey");
        m.setBlobKey(decoder0.decode(reader, m.getBlobKey()));
        reader = rootReader.newObjectReader("category");
        m.setCategory(decoder0.decode(reader, m.getCategory()));
        reader = rootReader.newObjectReader("description");
        m.setDescription(decoder0.decode(reader, m.getDescription()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("title");
        m.setTitle(decoder0.decode(reader, m.getTitle()));
        reader = rootReader.newObjectReader("updatedAt");
        m.setUpdatedAt(decoder0.decode(reader, m.getUpdatedAt()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}