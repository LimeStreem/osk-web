package osk.web.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-12-21 17:05:19")
/** */
public final class RoleGlueMeta extends org.slim3.datastore.ModelMeta<osk.web.model.RoleGlue> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.RoleGlue, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.RoleGlue, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.ModelRefAttributeMeta<osk.web.model.RoleGlue, org.slim3.datastore.ModelRef<osk.web.model.Profile>, osk.web.model.Profile> profileRef = new org.slim3.datastore.ModelRefAttributeMeta<osk.web.model.RoleGlue, org.slim3.datastore.ModelRef<osk.web.model.Profile>, osk.web.model.Profile>(this, "profileRef", "profileRef", org.slim3.datastore.ModelRef.class, osk.web.model.Profile.class);

    /** */
    public final org.slim3.datastore.ModelRefAttributeMeta<osk.web.model.RoleGlue, org.slim3.datastore.ModelRef<osk.web.model.Role>, osk.web.model.Role> roleRef = new org.slim3.datastore.ModelRefAttributeMeta<osk.web.model.RoleGlue, org.slim3.datastore.ModelRef<osk.web.model.Role>, osk.web.model.Role>(this, "roleRef", "roleRef", org.slim3.datastore.ModelRef.class, osk.web.model.Role.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.RoleGlue, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.RoleGlue, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final RoleGlueMeta slim3_singleton = new RoleGlueMeta();

    /**
     * @return the singleton
     */
    public static RoleGlueMeta get() {
       return slim3_singleton;
    }

    /** */
    public RoleGlueMeta() {
        super("RoleGlue", osk.web.model.RoleGlue.class);
    }

    @Override
    public osk.web.model.RoleGlue entityToModel(com.google.appengine.api.datastore.Entity entity) {
        osk.web.model.RoleGlue model = new osk.web.model.RoleGlue();
        model.setKey(entity.getKey());
        if (model.getProfileRef() == null) {
            throw new NullPointerException("The property(profileRef) is null.");
        }
        model.getProfileRef().setKey((com.google.appengine.api.datastore.Key) entity.getProperty("profileRef"));
        if (model.getRoleRef() == null) {
            throw new NullPointerException("The property(roleRef) is null.");
        }
        model.getRoleRef().setKey((com.google.appengine.api.datastore.Key) entity.getProperty("roleRef"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        osk.web.model.RoleGlue m = (osk.web.model.RoleGlue) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        if (m.getProfileRef() == null) {
            throw new NullPointerException("The property(profileRef) must not be null.");
        }
        entity.setProperty("profileRef", m.getProfileRef().getKey());
        if (m.getRoleRef() == null) {
            throw new NullPointerException("The property(roleRef) must not be null.");
        }
        entity.setProperty("roleRef", m.getRoleRef().getKey());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        osk.web.model.RoleGlue m = (osk.web.model.RoleGlue) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        osk.web.model.RoleGlue m = (osk.web.model.RoleGlue) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        osk.web.model.RoleGlue m = (osk.web.model.RoleGlue) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
        osk.web.model.RoleGlue m = (osk.web.model.RoleGlue) model;
        if (m.getProfileRef() == null) {
            throw new NullPointerException("The property(profileRef) must not be null.");
        }
        m.getProfileRef().assignKeyIfNecessary(ds);
        if (m.getRoleRef() == null) {
            throw new NullPointerException("The property(roleRef) must not be null.");
        }
        m.getRoleRef().assignKeyIfNecessary(ds);
    }

    @Override
    protected void incrementVersion(Object model) {
        osk.web.model.RoleGlue m = (osk.web.model.RoleGlue) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        osk.web.model.RoleGlue m = (osk.web.model.RoleGlue) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getProfileRef() != null && m.getProfileRef().getKey() != null){
            writer.setNextPropertyName("profileRef");
            encoder0.encode(writer, m.getProfileRef(), maxDepth, currentDepth);
        }
        if(m.getRoleRef() != null && m.getRoleRef().getKey() != null){
            writer.setNextPropertyName("roleRef");
            encoder0.encode(writer, m.getRoleRef(), maxDepth, currentDepth);
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected osk.web.model.RoleGlue jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        osk.web.model.RoleGlue m = new osk.web.model.RoleGlue();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("profileRef");
        decoder0.decode(reader, m.getProfileRef(), maxDepth, currentDepth);
        reader = rootReader.newObjectReader("roleRef");
        decoder0.decode(reader, m.getRoleRef(), maxDepth, currentDepth);
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}