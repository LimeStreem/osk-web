package osk.web.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2013-12-21 17:05:19")
/** */
public final class AuthorizationMeta extends org.slim3.datastore.ModelMeta<osk.web.model.Authorization> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Authorization> id = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Authorization>(this, "id", "id");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Authorization, java.lang.Boolean> isAdmin = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Authorization, java.lang.Boolean>(this, "isAdmin", "isAdmin", java.lang.Boolean.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<osk.web.model.Authorization, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<osk.web.model.Authorization, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<osk.web.model.Authorization> pass = new org.slim3.datastore.StringAttributeMeta<osk.web.model.Authorization>(this, "pass", "pass");

    /** */
    public final org.slim3.datastore.ModelRefAttributeMeta<osk.web.model.Authorization, org.slim3.datastore.ModelRef<osk.web.model.Profile>, osk.web.model.Profile> profile = new org.slim3.datastore.ModelRefAttributeMeta<osk.web.model.Authorization, org.slim3.datastore.ModelRef<osk.web.model.Profile>, osk.web.model.Profile>(this, "profile", "profile", org.slim3.datastore.ModelRef.class, osk.web.model.Profile.class);

    private static final AuthorizationMeta slim3_singleton = new AuthorizationMeta();

    /**
     * @return the singleton
     */
    public static AuthorizationMeta get() {
       return slim3_singleton;
    }

    /** */
    public AuthorizationMeta() {
        super("Authorization", osk.web.model.Authorization.class);
    }

    @Override
    public osk.web.model.Authorization entityToModel(com.google.appengine.api.datastore.Entity entity) {
        osk.web.model.Authorization model = new osk.web.model.Authorization();
        model.setId((java.lang.String) entity.getProperty("id"));
        model.setIsAdmin((java.lang.Boolean) entity.getProperty("isAdmin"));
        model.setKey(entity.getKey());
        model.setPass(decrypt((java.lang.String)entity.getProperty("pass")));
        if (model.getProfile() == null) {
            throw new NullPointerException("The property(profile) is null.");
        }
        model.getProfile().setKey((com.google.appengine.api.datastore.Key) entity.getProperty("profile"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        osk.web.model.Authorization m = (osk.web.model.Authorization) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("id", m.getId());
        entity.setProperty("isAdmin", m.getIsAdmin());
        entity.setProperty("pass", encrypt(m.getPass()));
        if (m.getProfile() == null) {
            throw new NullPointerException("The property(profile) must not be null.");
        }
        entity.setProperty("profile", m.getProfile().getKey());
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        osk.web.model.Authorization m = (osk.web.model.Authorization) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        osk.web.model.Authorization m = (osk.web.model.Authorization) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        throw new IllegalStateException("The version property of the model(osk.web.model.Authorization) is not defined.");
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
        osk.web.model.Authorization m = (osk.web.model.Authorization) model;
        if (m.getProfile() == null) {
            throw new NullPointerException("The property(profile) must not be null.");
        }
        m.getProfile().assignKeyIfNecessary(ds);
    }

    @Override
    protected void incrementVersion(Object model) {
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        if ("pass".equals(propertyName)) return true;
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        osk.web.model.Authorization m = (osk.web.model.Authorization) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getIsAdmin() != null){
            writer.setNextPropertyName("isAdmin");
            encoder0.encode(writer, m.getIsAdmin());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getPass() != null){
            writer.setNextPropertyName("pass");
            encoder0.encode(writer, encrypt(m.getPass()));
        }
        if(m.getProfile() != null && m.getProfile().getKey() != null){
            writer.setNextPropertyName("profile");
            encoder0.encode(writer, m.getProfile(), maxDepth, currentDepth);
        }
        writer.endObject();
    }

    @Override
    protected osk.web.model.Authorization jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        osk.web.model.Authorization m = new osk.web.model.Authorization();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("isAdmin");
        m.setIsAdmin(decoder0.decode(reader, m.getIsAdmin()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("pass");
        if(reader.read() != null){
            reader = new org.slim3.datastore.json.JsonValueReader(decrypt(reader.read()), rootReader.getModelReader());
        }
        m.setPass(decoder0.decode(reader, m.getPass()));
        reader = rootReader.newObjectReader("profile");
        decoder0.decode(reader, m.getProfile(), maxDepth, currentDepth);
        return m;
    }
}