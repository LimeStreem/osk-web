package osk.web.controller.member;


import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.controller.auth.AccountHelper;
import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class MetaController extends AuthBaseController {


    @Override
    protected int getControllerLayer() {
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        AccountHelper.SetAuthCipherKey();
        response.setContentType("text");
        AuthorizationMeta meta=AuthorizationMeta.get();
        Authorization auth=Datastore.query(meta).filter(meta.id.equal(userId)).asSingle();
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("user", userId);
        jsonObject.put("icon",AccountHelper.getGravaterAddress(userId, 32));
        jsonObject.put("isAdmin", auth.getIsAdmin().toString());
        response.getWriter().write(jsonObject.toString());
        return null;
    }

}
