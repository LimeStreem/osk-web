package osk.web.controller.member;

import org.slim3.controller.Navigation;

import osk.web.controller.AuthBaseController;

public class IndexController extends AuthBaseController {

    @Override
    protected Navigation authedRun(String userId) throws Exception
    {
        return forward("member.html");
    }

    @Override
    protected int getControllerLayer() {
        return 1;
    }
}
