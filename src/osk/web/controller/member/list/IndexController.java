package osk.web.controller.member.list;

import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.auth.AccountHelper;
import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;
import osk.web.model.Profile;
import osk.web.model.RoleGlue;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class IndexController extends Controller {

    @Override
    public Navigation run() throws Exception {
        response.setCharacterEncoding("UTF-8");
        String paramString=request.getParameter("type");
        int param=paramString==null?2:Integer.parseInt(paramString);
        AccountHelper.SetAuthCipherKey();
        AuthorizationMeta authMeta=AuthorizationMeta.get();
        List<Authorization> authes=Datastore.query(authMeta).asList();
        int count=0;
        if(param==0)
        {
            for(int i=0;i<authes.size();i++)
            {
                if(!authes.get(i).getProfile().getModel().getIsOB())
                {
                    count++;
                }
            }
        }else if (param==1)
        {
            for(int i=0;i<authes.size();i++)
            {
                if(authes.get(i).getProfile().getModel().getIsOB())
                {
                    count++;
                }
            }
        }else {
            count=authes.size();
        }
        ArrayList<String> icons=new ArrayList<String>();
        ArrayList<String> department = new ArrayList<String>();
        ArrayList<Long> ids=new ArrayList<Long>();
        ArrayList<String> major = new ArrayList<String>();
        ArrayList<String> names=new ArrayList<String>();
        ArrayList<Integer> counts=new ArrayList<Integer>();
        ArrayList<ArrayList<String>> type=new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> roleNames=new ArrayList<ArrayList<String>>();
        JSONObject json=new JSONObject();

        json.put("count",count);
        for(Authorization auth:authes)
        {
            Profile profile=auth.getProfile().getModel();
            if(param==0&&profile.getIsOB())
            {
                continue;
            }else if(param==1&&!profile.getIsOB())
            {
                continue;
            }
            icons.add(AccountHelper.getGravaterAddress(auth.getId(), 32));
            department.add(profile.getDepartment());
            ids.add(auth.getKey().getId());
            major.add(profile.getMajor());
            names.add(profile.getName());
            List<RoleGlue> glues=profile.getRoleRef().getModelList();
            counts.add(glues.size());
            roleNames.add(new ArrayList<String>());
            type.add(new ArrayList<String>());
            for (RoleGlue roleGlue : glues) {
                roleNames.get(roleNames.size()-1).add(roleGlue.getRoleRef().getModel().getRoleName());
                type.get(type.size()-1).add(roleGlue.getRoleRef().getModel().getType());
            }
        }
        json.put("icons", icons);
        json.put("department", department);
        json.put("major", major);
        json.put("names", names);
        json.put("ids", ids);
        json.put("counts", counts);
        json.put("roles", roleNames);
        json.put("types", type);
        response.getWriter().write(
            json.toString());
        return null;
    }
}
