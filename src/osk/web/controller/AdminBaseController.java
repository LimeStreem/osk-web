package osk.web.controller;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.auth.AccountHelper;
import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;

public abstract class AdminBaseController extends AuthBaseController {

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        AccountHelper.SetAuthCipherKey();
        AuthorizationMeta meta=AuthorizationMeta.get();
        Authorization authorization=Datastore.query(meta).filter(meta.id.equal(userId)).asSingle();
        if(authorization.getIsAdmin())
        {
            return adminRun(userId);
        }else {
            return getErrorNavigation("401c.html");
        }
    }

    protected abstract Navigation adminRun(String userId) throws Exception;

}
