package osk.web.controller.auth;

import org.slim3.controller.Navigation;

import osk.web.controller.AuthBaseController;

public class LogoutController extends AuthBaseController {


    @Override
    protected Navigation authedRun(String userId) throws Exception {
        //セッションを削除
        removeSessionScope(AccountHelper.verifyKeySessionName);
        removeSessionScope(AccountHelper.idSessionName);
        //ログアウトのページを表示
        return forward("logout.html");
    }

    @Override
    protected int getControllerLayer() {
        // TODO 自動生成されたメソッド・スタブ
        return 1;
    }


}
