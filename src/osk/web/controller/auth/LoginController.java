package osk.web.controller.auth;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class LoginController extends Controller {

    @Override
    public Navigation run() throws Exception {
        response.setContentType("text");
        if(AccountHelper.checkIsValidUser(request))
        {
            sessionScope(AccountHelper.idSessionName,request.getParameter("id"));
            sessionScope("verifyKey", AccountHelper.encrypt(AccountHelper.sessionEncryptKey, request.getRemoteAddr()+request.getParameter("id")));
            return verifiedUser();
        }else {
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("status","failed");
            response.getWriter().write(jsonObject.toString());
            return null;
        }
    }

    protected Navigation verifiedUser() throws Exception
    {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("status","success");
        response.getWriter().write(jsonObject.toString());
        return null;
    }
}
