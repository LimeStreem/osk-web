package osk.web.controller.auth;

import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class UsersController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception
    {
        response.setCharacterEncoding("UTF-8");
        JSONObject jsonObject=new JSONObject();
        List<String> idList=new ArrayList<String>();
        List<String> nameList=new ArrayList<String>();
        AccountHelper.SetAuthCipherKey();
        AuthorizationMeta meta=AuthorizationMeta.get();
        List<Authorization> accounts=Datastore.query(meta).asList();
        jsonObject.put("count", accounts.size());
        for(Authorization account:accounts)
        {
            idList.add(account.getId());
            nameList.add(account.getProfile().getModel().getName());
        }
        jsonObject.put("ids",idList);
        jsonObject.put("names", nameList);
        response.getWriter().write(jsonObject.toString());
        return null;
    }
}
