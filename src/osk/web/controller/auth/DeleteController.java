package osk.web.controller.auth;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AdminBaseController;
import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class DeleteController extends AdminBaseController {

    @Override
    protected Navigation adminRun(String userId) throws Exception {
        JSONObject jsonObject=new JSONObject();
        AuthorizationMeta meta=AuthorizationMeta.get();
        Authorization target=Datastore.query(meta).filter(meta.id.equal(request.getParameter("id"))).asSingle();
        if(target.getIsAdmin())
        {
            //管理者の場合は削除できないようにする
            jsonObject.put("status","error:isAdmin");
            response.getWriter().write(jsonObject.toString());
            return null;
        }
        Datastore.delete(target.getProfile().getKey());
        Datastore.delete(target.getKey());
        jsonObject.put("status", "success");
        response.getWriter().write(jsonObject.toString());
        return null;
    }

    @Override
    protected int getControllerLayer() {
        return 1;
    }

}
