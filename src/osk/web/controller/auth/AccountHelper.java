package osk.web.controller.auth;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.slim3.datastore.Datastore;

import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;

public class AccountHelper {

    /***
     * 暗号化キー
     */
    private static String ciperKey="AF3SU578JQKN7DEP";
/*
     * IDのセッションキー
     */
    public static String idSessionName="IdSession";

    public static String verifyKeySessionName="verifyKey";

    /*
     * セッションの暗号化キー
     */
    public static String sessionEncryptKey="AKHDNESJKQLMSK@A";

    public static void SetAuthCipherKey()
    {
        Datastore.setLimitedCipherKey(ciperKey);
    }

    public static boolean checkIsValidUser(HttpServletRequest request)
    {
        //POSTで受け渡されたidとpassの内容を取得する
        String id=request.getParameter("id");
        String pass=request.getParameter("pass");

        if(id.equals("")||pass.equals(""))
        {
            return false;
        }

        //暗号化キーをセット
        Datastore.setLimitedCipherKey(ciperKey);

        //IDとパスワードが一致するユーザーデータを取得
        AuthorizationMeta authMeta=AuthorizationMeta.get();
        Authorization authData=Datastore.query(authMeta).filter(authMeta.id.equal(id),authMeta.pass.equal(pass)).asSingle();

        if(authData==null)
        {
            return false;//ログイン失敗とする
        }else
        {
            return true;//ログイン成功とする
        }
    }

    public static void subscribeNewUser(String id,String pass)
    {
        Datastore.setLimitedCipherKey(ciperKey);
        Authorization auth=new Authorization();
        auth.setId(id);
        auth.setPass(pass);
        Datastore.put(auth);
    }

    public static byte[]   encrypt(String key, String text)
            throws javax.crypto.IllegalBlockSizeException,
                java.security.InvalidKeyException,
                java.security.NoSuchAlgorithmException,
                java.io.UnsupportedEncodingException,
                javax.crypto.BadPaddingException,
                javax.crypto.NoSuchPaddingException
    {
        javax.crypto.spec.SecretKeySpec sksSpec =
            new javax.crypto.spec.SecretKeySpec(key.getBytes(), "Blowfish");
        javax.crypto.Cipher cipher =
            javax.crypto.Cipher.getInstance("Blowfish");
        cipher.init(javax.crypto.Cipher.ENCRYPT_MODE, sksSpec);
        byte[] encrypted = cipher.doFinal(text.getBytes());
        return encrypted;
    }

    public static String   decrypt(String key, byte[] encrypted)
            throws javax.crypto.IllegalBlockSizeException,
                java.security.InvalidKeyException,
                java.security.NoSuchAlgorithmException,
                java.io.UnsupportedEncodingException,
                javax.crypto.BadPaddingException,
                javax.crypto.NoSuchPaddingException
    {
        javax.crypto.spec.SecretKeySpec sksSpec =
            new javax.crypto.spec.SecretKeySpec(key.getBytes(), "Blowfish");
        javax.crypto.Cipher cipher =
            javax.crypto.Cipher.getInstance("Blowfish");
        cipher.init(javax.crypto.Cipher.DECRYPT_MODE, sksSpec);
        byte[] decrypted = cipher.doFinal(encrypted);
        return new String(decrypted);
    }

    private static String hex(byte[] array) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
        sb.append(Integer.toHexString((array[i]
            & 0xFF) | 0x100).substring(1,3));
        }
        return sb.toString();
    }

    private static String md5Hex (String message) {
        try {
        MessageDigest md =
            MessageDigest.getInstance("MD5");
        return hex (md.digest(message.getBytes("CP1252")));
        } catch (NoSuchAlgorithmException e) {
        } catch (UnsupportedEncodingException e) {
        }
        return null;
    }

    public static String getGravaterAddress(String userId,Integer size)
    {
        return "http://www.gravatar.com/avatar/"+md5Hex(userId.toLowerCase())+"?s="+size.toString();
    }

}
