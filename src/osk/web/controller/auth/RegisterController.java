package osk.web.controller.auth;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AdminBaseController;
import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;
import osk.web.model.Profile;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class RegisterController extends AdminBaseController {

    @Override
    protected Navigation adminRun(String userId) throws Exception {
        //パラメータを取得
        String id=request.getParameter("id");
        String pass=request.getParameter("pass");
        JSONObject jsonObject=new JSONObject();
        AccountHelper.SetAuthCipherKey();
        //登録モデルを生成
        Authorization authorization=new Authorization();
        AuthorizationMeta meta=AuthorizationMeta.get();
        if(Datastore.query(meta).filter(meta.id.equal(id)).count()>0)
        {
            jsonObject.put("status", "duplicated");
            response.getWriter().write(jsonObject.toString());
            return null;
        }else {
            jsonObject.put("status","success");
        }
        Profile profile=new Profile();
        authorization.setId(id);
        authorization.setPass(pass);
        profile.setMailAddress(id);
        authorization.getProfile().setModel(profile);
        Datastore.put(profile);
        Datastore.put(authorization);
        //メール送信処理
        Properties properties=new Properties();
        Session session=Session.getDefaultInstance(properties, null);
            MimeMessage msg=new MimeMessage(session);
            msg.setFrom(new InternetAddress("osk-kernel@osk-web.appspotmail.com", "OSK Kernel.net", "ISO-2022-JP"));
            msg.addRecipient(Message.RecipientType.TO,
                             new InternetAddress(id, "OSK Kernel.net Client", "ISO-2022-JP"));
            msg.setSubject("OSK Kernel.netの登録が完了しました", "ISO-2022-JP");
            msg.setContent("WELCOME TO OSK!!<br><br>応用数学研究部ホームページへの登録が完了しました。<br><br>       ID:"+id+"<br>活動連絡は応数研サイトの部員用ページで行います。<br>部員用ページには他にも各班の連絡板や過去の作品など、部員を補助する様々なコンテンツがあります。<br>積極的に活用してください。<br>以下のURLをブックマークしておいてください。<br><br>応数研HP    <a href=\"http://www.osk-web.appspot.com\">http://www.osk-web.appspot.com</a><br>今回登録した部員用ページへは、公式サイト上で「上上下下左右左右BA」と入力することによりそのページへのログインフォームを出すことができます。<br><br>Internet Explorer 8以前では正しく表示できないことが多いので注意してください。","text/html");
            Transport.send(msg);

        response.getWriter().write(jsonObject.toString());
        return null;
    }


    @Override
    protected int getControllerLayer() {
        // TODO 自動生成されたメソッド・スタブ
        return 1;
    }
}
