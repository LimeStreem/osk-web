package osk.web.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Logger;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import osk.web.controller.auth.AccountHelper;

public abstract class AuthBaseController extends Controller
{
    protected Logger logger;

    @Override
    protected Navigation run() throws Exception
    {
        logger=Logger.getLogger(this.getClass().getName());
        //検証用キーをセッションから取得する
        byte[] verifyKey=sessionScope("verifyKey");
        if(verifyKey==null)
        {
            //キーを持っていないのでセッションエラー?
            return getErrorNavigation("401.html");
        }
        //復号化した検証用キー
        String decryptedString=AccountHelper.decrypt(AccountHelper.sessionEncryptKey, verifyKey);
        String userId=sessionScope(AccountHelper.idSessionName);
        if(userId!=null&&decryptedString.equals(request.getRemoteAddr()+userId)&&!userId.isEmpty())
        {
            logger.info("検証OK:"+userId);
            return authedRun(userId);
        }else
        {
            return getErrorNavigation("401b.html");
        }
    }

    protected Navigation getErrorNavigation(String errorFile) {
        StringBuilder builder=new StringBuilder();
        for (int i = 0; i < getControllerLayer(); i++)
        {
            builder.append("../");
        }
        return forward(builder.toString()+"error/"+errorFile);
    }

    /**
     * コントローラーの階層
     *\/・・・0
     *\/auth\/・・・・1
     * @return
     */
    protected abstract int getControllerLayer();

    protected abstract Navigation authedRun(String userId) throws Exception;

    protected String getURLDecodedParamater(String id) throws UnsupportedEncodingException
    {
        String str=request.getParameter(id);
        if(str!=null)
        return URLDecoder.decode(str,"utf-8");
        else {
            return "";
        }
    }
}
