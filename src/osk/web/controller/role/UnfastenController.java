package osk.web.controller.role;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AdminBaseController;
import osk.web.model.Authorization;
import osk.web.model.RoleGlue;

public class UnfastenController extends AdminBaseController {

    @Override
    protected Navigation adminRun(String userId) throws Exception {
        long userKey=Long.parseLong(request.getParameter("user"));
        long roleKey=Long.parseLong(request.getParameter("role"));
        Authorization  authorization=Datastore.get(Authorization.class,Datastore.createKey(Authorization.class, userKey));
        for (RoleGlue glue : authorization.getProfile().getModel().getRoleRef().getModelList())
        {
            if(glue.getRoleRef().getModel().getKey().getId()==roleKey)
            {
                Datastore.delete(glue.getKey());
            }
        }
        return null;
    }

    @Override
    protected int getControllerLayer() {
        // TODO 自動生成されたメソッド・スタブ
        return 1;
    }


}
