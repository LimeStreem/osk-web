package osk.web.controller.role;

import java.net.URLDecoder;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.meta.RoleMeta;
import osk.web.model.Role;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class AddController extends Controller {

    @Override
    public Navigation run() throws Exception {
        String roleName=URLDecoder.decode(request.getParameter("name"),"utf-8");
        String type=URLDecoder.decode(request.getParameter("type"),"utf-8");
        RoleMeta roleMeta=RoleMeta.get();
        JSONObject jsonObject=new JSONObject();
        if(Datastore.query(roleMeta).filter(roleMeta.roleName.equal(roleName)).count()==0){
        Role role=new Role();
        role.setRoleName(roleName);
        role.setType(type);
        Datastore.put(role);
            jsonObject.put("status", "success");
        }else {
            jsonObject.put("status", "dupe");
        }
        response.getWriter().write(jsonObject.toString());
        return null;
    }
}
