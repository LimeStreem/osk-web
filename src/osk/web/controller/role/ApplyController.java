package osk.web.controller.role;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.auth.AccountHelper;
import osk.web.model.Authorization;
import osk.web.model.Role;
import osk.web.model.RoleGlue;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class ApplyController extends Controller {

    @Override
    public Navigation run() throws Exception {
        AccountHelper.SetAuthCipherKey();
        Authorization authorization=Datastore.get(Authorization.class,Datastore.createKey(Authorization.class,Long.parseLong(request.getParameter("id"))));
        Role role=Datastore.get(Role.class,Datastore.createKey(Role.class,Long.parseLong(request.getParameter("tag"))));
        RoleGlue glue=new RoleGlue();
        glue.getProfileRef().setKey(authorization.getProfile().getKey());
        glue.getRoleRef().setKey(role.getKey());
        Datastore.put(glue);
        JSONObject jsonObject=new JSONObject();
        response.getWriter().write(jsonObject.toString());
        return null;
    }
}
