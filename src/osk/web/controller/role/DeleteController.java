package osk.web.controller.role;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.model.Role;
import osk.web.model.RoleGlue;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class DeleteController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        long id=Long.parseLong(request.getParameter("id"));
        Role role=Datastore.query(Role.class,Datastore.createKey(Role.class, id)).asSingle();
        if(role.getProfileListRef()!=null)
        for(RoleGlue glue:role.getProfileListRef().getModelList())
        {
            Datastore.delete(glue.getKey());
        }
        Datastore.delete(role.getKey());
        JSONObject jsonObject=new JSONObject();
        response.getWriter().write(jsonObject.toString());
        return null;
    }

}
