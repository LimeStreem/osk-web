package osk.web.controller.role;

import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.meta.RoleMeta;
import osk.web.model.Role;
import osk.web.model.RoleGlue;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class ListController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        // TODO 自動生成されたメソッド・スタブ
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        response.setCharacterEncoding("utf-8");
        RoleMeta roleMeta=RoleMeta.get();
        List<Role> roles=Datastore.query(roleMeta).asList();
        List<String> names=new ArrayList<String>();
        List<Long> ids=new ArrayList<Long>();
        List<Integer> counts=new ArrayList<Integer>();
        List<String> types=new ArrayList<String>();
        List<List<String>> namesList=new ArrayList<List<String>>();

        for(int i=0;i<roles.size();i++)
        {
            namesList.add(new ArrayList<String>());
        }

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count", roles.size());
        int c=0;
        for(Role role:roles)
        {
            names.add(role.getRoleName());
            ids.add(role.getKey().getId());
            List<RoleGlue> glues=role.getProfileListRef().getModelList();
            counts.add(glues.size());
            types.add(role.getType());
            for (RoleGlue roleGlue : glues) {
                namesList.get(c).add(roleGlue.getProfileRef().getModel().getName());
            }
            c++;
        }
        jsonObject.put("names", names);
        jsonObject.put("ids", ids);
        jsonObject.put("appliedNames", namesList);
        jsonObject.put("namesCounts", counts);
        jsonObject.put("type", types);
        response.getWriter().write(jsonObject.toString());
        return null;
    }


}
