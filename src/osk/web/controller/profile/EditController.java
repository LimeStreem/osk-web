package osk.web.controller.profile;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.controller.auth.AccountHelper;
import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;
import osk.web.model.Profile;

import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class EditController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        AccountHelper.SetAuthCipherKey();
        AuthorizationMeta authMeta=AuthorizationMeta.get();
        Authorization authorization=Datastore.query(authMeta).filter(authMeta.id.equal(userId)).asSingle();
        Profile profile=authorization.getProfile().getModel();
        profile.setName(request.getParameter("name"));
        profile.setNickName(request.getParameter("nickName"));
        profile.setMailAddress(request.getParameter("mailAddress"));
        profile.setSchoolNumber(request.getParameter("schoolNumber"));
        profile.setDepartment(request.getParameter("department"));
        profile.setMajor(request.getParameter("major"));
        profile.setUsableLanguage(request.getParameter("usableLanguage"));
        profile.setFavoriteAnime(request.getParameter("favoriteAnime"));
        profile.setFavoriteFormula(request.getParameter("favoriteFormula"));
        profile.setSelfIntroduction(new Text(request.getParameter("selfIntroduction")));
        profile.setIsOB(Boolean.parseBoolean(request.getParameter("isOB")));
        profile.setIsMale(Boolean.parseBoolean(request.getParameter("isMale")));
        profile.setZipCode(request.getParameter("zipCode"));
        profile.setAddress(request.getParameter("address"));
        profile.setGithubURL(request.getParameter("githubURL"));
        profile.setBitBucketURL(request.getParameter("bitBucketURL"));
        profile.setTwitterUserName(request.getParameter("twitterUserName"));
        profile.setUrl(request.getParameter("url"));
        Datastore.put(profile);
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("status","success");
        response.getWriter().write(jsonObject.toString());
        return null;
    }

}
