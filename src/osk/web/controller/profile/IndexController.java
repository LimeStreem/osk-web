package osk.web.controller.profile;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.controller.auth.AccountHelper;
import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;
import osk.web.model.Profile;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class IndexController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        return 1;
    }
//http://localhost:8888/AAA/XXX
  //→osk.web.controller.AAA.XXXControler
   //if(XXX=="")→osk.web.controller.AAA.IndexController
// profile/←アドレスを省略 http://localhost:8888/AAA/XXX/profile/
    // /profile/←ドメインを省略 http://localhost:8888/profile/
    @Override
    protected Navigation authedRun(String userId) throws Exception {
        response.setCharacterEncoding("UTF-8");
        AccountHelper.SetAuthCipherKey();
        AuthorizationMeta meta=AuthorizationMeta.get();
        Authorization authorization=Datastore.query(meta).filter(meta.id.equal(userId)).asSingle();
        Profile profile=authorization.getProfile().getModel();
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name",profile.getName());
        jsonObject.put("nickName",profile.getNickName());
        jsonObject.put("mailAddress",profile.getMailAddress());
        jsonObject.put("schoolNumber",profile.getSchoolNumber());
        jsonObject.put("department", profile.getDepartment());
        jsonObject.put("major", profile.getMajor());
        jsonObject.put("usableLanguage",profile.getUsableLanguage());
        jsonObject.put("favoriteAnime",profile.getFavoriteAnime());
        jsonObject.put("favoriteFormula",profile.getFavoriteFormula());
        jsonObject.put("selfIntroduction",profile.getSelfIntroduction()==null?"":profile.getSelfIntroduction().getValue());
        jsonObject.put("isOB",profile.getIsOB());
        jsonObject.put("isMale",profile.getIsMale());
        jsonObject.put("zipCode",profile.getZipCode());
        jsonObject.put("address",profile.getAddress());
        jsonObject.put("githubURL",profile.getGithubURL());
        jsonObject.put("bitBucketURL",profile.getBitBucketURL());
        jsonObject.put("twitterUserName",profile.getTwitterUserName());
        jsonObject.put("url",profile.getUrl());
        response.getWriter().write(jsonObject.toString());
        return null;
    }
}
