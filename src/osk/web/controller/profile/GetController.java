package osk.web.controller.profile;

import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.controller.auth.AccountHelper;
import osk.web.model.Authorization;
import osk.web.model.Profile;
import osk.web.model.RoleGlue;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class GetController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        response.setCharacterEncoding("utf-8");
        AccountHelper.SetAuthCipherKey();
            String strId=request.getParameter("id");
            List<String> types=new ArrayList<String>();
            List<String> roles=new ArrayList<String>();
            Long id=Long.parseLong(strId);
            Authorization authorization=Datastore.get(Authorization.class,Datastore.createKey(Authorization.class, id));
            JSONObject jsonObject=new JSONObject();
            Profile profile=authorization.getProfile().getModel();
            List<RoleGlue> roleModels=profile.getRoleRef().getModelList();
            jsonObject.put("icon", AccountHelper.getGravaterAddress(authorization.getId(), 128));
            jsonObject.put("name",profile.getName() );
            jsonObject.put("nick", profile.getNickName());
            jsonObject.put("major", profile.getMajor());
            jsonObject.put("department", profile.getDepartment());
            jsonObject.put("usableLanguage", profile.getUsableLanguage());
            jsonObject.put("favoriteAnime", profile.getFavoriteAnime());
            jsonObject.put("selfIntroduction",profile.getSelfIntroduction()==null?"":profile.getSelfIntroduction().getValue());
            jsonObject.put("favoriteFormula", profile.getFavoriteFormula());
            jsonObject.put("twitter", profile.getTwitterUserName());
            jsonObject.put("github", profile.getGithubURL());
            jsonObject.put("bitbucket", profile.getBitBucketURL());
            jsonObject.put("url", profile.getUrl());
            jsonObject.put("isOB", profile.getIsOB());
            jsonObject.put("count", roleModels.size());
            for(RoleGlue roleGlue:roleModels)
            {
                types.add(roleGlue.getRoleRef().getModel().getType());
                roles.add(roleGlue.getRoleRef().getModel().getRoleName());
            }
            jsonObject.put("types", types);
            jsonObject.put("roles", roles);
            response.getWriter().write(jsonObject.toString());
        return null;
    }

}
