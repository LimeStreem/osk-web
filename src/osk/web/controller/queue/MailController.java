package osk.web.controller.queue;

import java.net.URLDecoder;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

public class MailController extends Controller {

    Logger logger;
    @Override
    public Navigation run() throws Exception {
        logger=Logger.getLogger(this.getClass().getName());
        StringBuilder logTextBuilder=new StringBuilder();
        logTextBuilder.append("メール送信処理タスクがリクエストされました。\n");
        String to=request.getParameter("to");
        String title=URLDecoder.decode(request.getParameter("title"),"utf-8");
        String content=URLDecoder.decode(request.getParameter("content"),"utf-8");
        logTextBuilder.append("送信先:"+to+"\n");
        logTextBuilder.append("タイトル:"+title+"\n");
        logTextBuilder.append("内容:"+content+"\n");
        Properties properties=new Properties();
        Session session=Session.getDefaultInstance(properties, null);
            MimeMessage msg=new MimeMessage(session);
            msg.setFrom(new InternetAddress("osk-kernel@osk-web.appspotmail.com", "OSK Kernel.net", "ISO-2022-JP"));
            msg.addRecipient(Message.RecipientType.TO,
                             new InternetAddress(to, "OSK Kernel.net Client", "ISO-2022-JP"));
            msg.setSubject(title, "ISO-2022-JP");
            msg.setContent(content,"text/html");
            Transport.send(msg);
            logTextBuilder.append("送信完了.");
        logger.info(logTextBuilder.toString());
        return null;
    }
}
