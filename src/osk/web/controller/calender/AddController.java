package osk.web.controller.calender;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.model.Schedule;

import com.google.appengine.api.datastore.Text;

public class AddController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {

        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        Schedule schedule=new Schedule();
        schedule.setTitle(getURLDecodedParamater("title"));
        schedule.setContent(new Text(getURLDecodedParamater("desc")));
        //schedule.setMonth(Integer.parseInt(getURLDecodedParamater("month")));
        schedule.setRoleTag(getURLDecodedParamater("role"));
        schedule.setTag(getURLDecodedParamater("tag"));
        String date=getURLDecodedParamater("date");
        String[] splited=date.split("/");
        String replaced=date.replace("/", "-");
        schedule.setMonth(Integer.parseInt(splited[0]));
        schedule.setDate(replaced);
        Datastore.put(schedule);
        return  null;
    }

}
