package osk.web.controller.calender;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.meta.ScheduleMeta;
import osk.web.model.Schedule;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class GetController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        // TODO 自動生成されたメソッド・スタブ
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        int month=Integer.parseInt(getURLDecodedParamater("month"));
        ScheduleMeta meta=ScheduleMeta.get();
        List<Schedule> schedules=Datastore.query(meta).filter(meta.month.equal(month)).asList();
        ArrayList<String> names=new ArrayList<String>();
        ArrayList<String> dates=new ArrayList<String>();
        ArrayList<Long> ids=new ArrayList<Long>();
        for(Schedule schedule:schedules)
        {
            names.add(URLEncoder.encode(schedule.getTag(), "utf-8"));
            dates.add(URLEncoder.encode(schedule.getDate(),"utf-8"));
            ids.add(schedule.getKey().getId());
        }
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count", schedules.size());
        jsonObject.put("tags", names);
        jsonObject.put("dates", dates);
        response.getWriter().write(jsonObject.toString());
        return null;
    }

}
