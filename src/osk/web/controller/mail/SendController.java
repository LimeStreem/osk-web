package osk.web.controller.mail;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.controller.auth.AccountHelper;
import osk.web.meta.AuthorizationMeta;
import osk.web.meta.ProfileMeta;
import osk.web.meta.RoleMeta;
import osk.web.model.Authorization;
import osk.web.model.Profile;
import osk.web.model.Role;
import osk.web.model.RoleGlue;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class SendController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        // TODO 自動生成されたメソッド・スタブ
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        // TODO 自動生成されたメソッド・スタブ
        AccountHelper.SetAuthCipherKey();
        AuthorizationMeta authorizationMeta=AuthorizationMeta.get();
        ProfileMeta profileMeta=ProfileMeta.get();
        String rawTargetString=URLDecoder.decode(request.getParameter("target"),"utf-8");
        String title=URLDecoder.decode(request.getParameter("title"),"utf-8");
        String content=URLDecoder.decode(request.getParameter("content"),"utf-8");
        //ログ処理
        StringBuilder builder=new StringBuilder();
        builder.append("メール送信処理がリクエストされました。\nタイトル");
        builder.append(title);
        builder.append("\n内容:"+content);
        builder.append("対象:"+rawTargetString+"\n送信処理を開始。\n");
        String[] targets=rawTargetString.split(" ");
        if(rawTargetString.isEmpty())
        {
            builder.append("対象役職未指定のため、全体への送信と解釈。\n");
            List<Authorization> authes=Datastore.query(authorizationMeta).asList();
            Iterator<Authorization> itrAuth=authes.iterator();
            Authorization authorization=Datastore.query(authorizationMeta).filter(authorizationMeta.id.equal(userId)).asSingle();
            while(itrAuth.hasNext()){
                Authorization currentAuthorization=itrAuth.next();
            builder.append("To:"+currentAuthorization.getId()+"\n");
            Queue queue=QueueFactory.getQueue("mail-queue");
            TaskOptions to = TaskOptions.Builder.withUrl("/queue/mail").param("to", currentAuthorization.getId())
                .param("title",URLEncoder.encode("【OSK Kernel.net】"+title,"utf-8") ).param("content", URLEncoder.encode((URLDecoder.decode(authorization.getProfile().getModel().getName(),"utf-8")+"さんよりメッセージ<br><br>"+content),"utf-8"));
                queue.add(to.method(Method.POST));
            }
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("status", "success");
            response.getWriter().write(jsonObject.toString());
            logger.info(builder.toString());
            return null;
        }
        Set<String> set=new HashSet<String>();
        RoleMeta roleMeta=RoleMeta.get();
        for(String target:targets)
        {
            if(target.equals("現役"))
            {
                Iterator<Profile> profileIterator=Datastore.query(profileMeta).filter(profileMeta.isOB.equal(false)).asIterator();
                while(profileIterator.hasNext())
                {
                    Profile currentProfile=profileIterator.next();
                    set.add(currentProfile.getAuthorizationInverseModelRef().getModel().getId());
                }
            }else
            if(target.equals("OB"))
            {
                Iterator<Profile> profileIterator=Datastore.query(profileMeta).filter(profileMeta.isOB.equal(true)).asIterator();
                while(profileIterator.hasNext())
                {
                    Profile currentProfile=profileIterator.next();
                    set.add(currentProfile.getAuthorizationInverseModelRef().getModel().getId());
                }
            }else{
            Role role=Datastore.query(roleMeta).filter(roleMeta.roleName.equal(target)).asSingle();
            for(RoleGlue glue:role.getProfileListRef().getModelList())
            {
                set.add(glue.getProfileRef().getModel().getAuthorizationInverseModelRef().getModel().getId());
            }
            }
        }

        Iterator<String> itr=set.iterator();
        while(itr.hasNext())
        {
            String currentTargetString=itr.next();
            builder.append("To:"+currentTargetString+"\n");
            Authorization authorization=Datastore.query(authorizationMeta).filter(authorizationMeta.id.equal(userId)).asSingle();
        Queue queue=QueueFactory.getQueue("mail-queue");
        TaskOptions to = TaskOptions.Builder.withUrl("/queue/mail").param("to",currentTargetString)
                .param("title",URLEncoder.encode("【OSK Kernel.net】"+title,"utf-8") ).param("content", URLEncoder.encode((URLDecoder.decode(authorization.getProfile().getModel().getName(),"utf-8")+"さんよりメッセージ<br><br>"+content),"utf-8"));
        queue.add(to.method(Method.POST));
        }
        builder.append("送信完了");
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("status", "success");
        response.getWriter().write(jsonObject.toString());
        logger.info(builder.toString());
        return null;
    }

}
