package osk.web.controller.library;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.controller.auth.AccountHelper;
import osk.web.meta.AuthorizationMeta;
import osk.web.meta.LibraryContentMeta;
import osk.web.model.Authorization;
import osk.web.model.LibraryContent;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public abstract class LibraryWriteAuthBaseController extends AuthBaseController {

    protected BlobstoreService blobstoreService=BlobstoreServiceFactory.getBlobstoreService();

    @Override
    protected org.slim3.controller.Navigation authedRun(String userId) throws Exception
    {
        AccountHelper.SetAuthCipherKey();
        String blobString=request.getParameter("blobKey");
        BlobKey blobKey=new BlobKey(blobString);
        AuthorizationMeta meta=AuthorizationMeta.get();
        Authorization authorization=Datastore.query(meta).filter(meta.id.equal(userId)).asSingle();
        LibraryContentMeta contentMeta=LibraryContentMeta.get();
        LibraryContent content=Datastore.query(contentMeta).filter(contentMeta.blobKey.equal(blobString)).asSingle();
        if(content.getAutherId().equals(userId)||authorization.getIsAdmin())
        {
            return libraryAuthedRun(content,blobKey,authorization);
        }else
        {
            return getErrorNavigation("401b.html");
        }
    };

    protected abstract Navigation libraryAuthedRun(LibraryContent content, BlobKey blobKey, Authorization authorization)throws Exception;
}
