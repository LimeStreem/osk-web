package osk.web.controller.library;

import java.net.URLEncoder;

import org.slim3.controller.Navigation;

import osk.web.controller.AuthBaseController;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

public class DownloadController extends AuthBaseController {

    @Override
    protected int getControllerLayer()
    {
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception
    {
        String key=request.getParameter("blobKey");
        BlobKey blobKey=new BlobKey(key);
        BlobInfoFactory factory=new BlobInfoFactory();
        BlobInfo info=factory.loadBlobInfo(blobKey);
        String encodedFilename = URLEncoder.encode(info.getFilename(), "utf-8");
        encodedFilename.replaceAll("\\+", "%20");
        response.setHeader("Content-Disposition", "attachment;filename*=utf8''"+encodedFilename);
        BlobstoreService blobService=BlobstoreServiceFactory.getBlobstoreService();
        response.setContentType("octet-stream");
        blobService.serve(blobKey, response);
        return null;
    }
}
