package osk.web.controller.library;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.model.Authorization;
import osk.web.model.LibraryContent;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class DeleteController extends LibraryWriteAuthBaseController {

    @Override
    protected Navigation libraryAuthedRun(LibraryContent content,
            BlobKey blobKey,Authorization authorization)throws Exception {
        blobstoreService.delete(blobKey);
        Datastore.delete(content.getKey());
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("status", "success");
        response.getWriter().write(jsonObject.toString());
        return null;

    }

    @Override
    protected int getControllerLayer() {
        return 1;
    }


}
