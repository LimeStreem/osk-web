package osk.web.controller.library;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.model.LibraryContent;

import com.google.appengine.api.datastore.Key;

public class CancelController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        // TODO 自動生成されたメソッド・スタブ
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        String id=request.getParameter("id");
        Key key=Datastore.createKey(LibraryContent.class, Long.parseLong(id));
        Datastore.delete(key);
        return null;
    }

}
