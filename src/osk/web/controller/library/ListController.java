package osk.web.controller.library;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.controller.auth.AccountHelper;
import osk.web.meta.AuthorizationMeta;
import osk.web.meta.LibraryContentMeta;
import osk.web.model.Authorization;
import osk.web.model.LibraryContent;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class ListController extends AuthBaseController {

    private BlobInfoFactory serviceFactory=new BlobInfoFactory();
    @Override
    protected int getControllerLayer() {
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception
    {
        response.setCharacterEncoding("UTF-8");
        AccountHelper.SetAuthCipherKey();
        AuthorizationMeta authMeta=AuthorizationMeta.get();
        Authorization me=Datastore.query(authMeta).filter(authMeta.id.equal(userId)).asSingle();
        String category=request.getParameter("category");
        LibraryContentMeta meta=LibraryContentMeta.get();
        List<LibraryContent> contents=Datastore.query(meta).filter(meta.category.equal(category)).asList();
        List<String> keys=new ArrayList<String>();
        List<String> titles=new ArrayList<String>();
        List<String> descriptions=new ArrayList<String>();
        List<String> auther=new ArrayList<String>();
        List<String> hasRight=new ArrayList<String>();
        List<Long> sizes=new ArrayList<Long>();
        List<String> hashes=new ArrayList<String>();
        List<Date> updateDates=new ArrayList<Date>();
        JSONObject jsonObject=new JSONObject();
        for(LibraryContent content:contents)
        {
            BlobInfo info=serviceFactory.loadBlobInfo(new BlobKey(content.getBlobKey()));
            keys.add(content.getBlobKey());
            titles.add(content.getTitle());
            descriptions.add(content.getDescription());
            updateDates.add(content.getUpdatedAt());
            sizes.add(info!=null?info.getSize():0);
            hashes.add(info!=null?info.getMd5Hash():"");
            auther.add(content.getAutherProfile().getModel()==null?content.getAutherId():content.getAutherProfile().getModel().getName());
            if(me.getIsAdmin()||userId.equals(content.getAutherId()))
            {
                    hasRight.add("true");
            }else
            {
                hasRight.add("false");
            }
        }
        jsonObject.put("count",contents.size());
        jsonObject.put("blobKeys", keys);
        jsonObject.put("titles",titles);
        jsonObject.put("descs",descriptions);
        jsonObject.put("date",updateDates);
        jsonObject.put("auther", auther);
        jsonObject.put("hasRight",hasRight);
        jsonObject.put("sizes", sizes);
        jsonObject.put("hashes", hashes);
        response.getWriter().write(jsonObject.toString());
        return null;
    }

}
