package osk.web.controller.library;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.model.Authorization;
import osk.web.model.LibraryContent;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class EditController extends LibraryWriteAuthBaseController{

    @Override
    protected Navigation libraryAuthedRun(LibraryContent content,
            BlobKey blobKey,Authorization authorization) throws Exception {
        String title=request.getParameter("title");
        String desc=request.getParameter("desc");
        content.setAutherId(authorization.getId());
        content.setDescription(desc);
        content.setTitle(title);
        content.getAutherProfile().setKey(authorization.getProfile().getKey());
        Datastore.put(content);
        JSONObject json=new JSONObject();
        json.put("status", "success");
        response.getWriter().write(json.toString());
        return null;
    }

    @Override
    protected int getControllerLayer() {
        // TODO 自動生成されたメソッド・スタブ
        return 1;
    }
}
