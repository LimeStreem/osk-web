package osk.web.controller.library;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.meta.LibraryContentMeta;
import osk.web.model.LibraryContent;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class CompleteController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        BlobstoreService blobService=BlobstoreServiceFactory.getBlobstoreService();
        BlobKey blobKey=blobService.getUploads(request).get("file").get(0);
        String strId=request.getParameter("id");
        Key key=Datastore.createKey(LibraryContent.class, Long.parseLong(strId));
        LibraryContent content=Datastore.get(LibraryContentMeta.get(), key);
        content.setBlobKey(blobKey.getKeyString());
        Datastore.put(content);
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("status", "success");
        response.getWriter().write(jsonObject.toString());
        return null;
    }

}
