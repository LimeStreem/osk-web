package osk.web.controller.library;

import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.AuthBaseController;
import osk.web.controller.auth.AccountHelper;
import osk.web.meta.AuthorizationMeta;
import osk.web.model.Authorization;
import osk.web.model.LibraryContent;

import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class AddController extends AuthBaseController {

    @Override
    protected int getControllerLayer() {
        return 1;
    }

    @Override
    protected Navigation authedRun(String userId) throws Exception {
        AccountHelper.SetAuthCipherKey();
        JSONObject json=new JSONObject();
        AuthorizationMeta meta=AuthorizationMeta.get();
        Authorization me=Datastore.query(meta).filter(meta.id.equal(userId)).asSingle();
        String title=request.getParameter("title");
        String desc=request.getParameter("desc");
        String category=request.getParameter("category");
        if(title.isEmpty()||desc.isEmpty()||category.isEmpty())
        {
            json.put("status", "failed");
            response.getWriter().write(json.toString());
            return null;
        }
        LibraryContent content=new LibraryContent();
        content.setTitle(title);
        content.setDescription(desc);
        content.getAutherProfile().setKey(me.getProfile().getKey());
        content.setAutherId(userId);
        content.setCategory(category);
        Key key=Datastore.put(content);
        BlobstoreService blobstoreService=BlobstoreServiceFactory.getBlobstoreService();
        String uploadURL=blobstoreService.createUploadUrl("/library/complete?id="+key.getId());
        json.put("url", uploadURL);
        json.put("id", key.getId());
        response.getWriter().write(json.toString());
        return null;
    }

}
