package osk.web.controller.cron.day;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.datastore.Datastore;

import osk.web.controller.auth.AccountHelper;
import osk.web.meta.ProfileMeta;
import osk.web.meta.ScheduleMeta;
import osk.web.model.Profile;
import osk.web.model.RoleGlue;
import osk.web.model.Schedule;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.appengine.api.taskqueue.TaskOptions.Method;

public class ScheduleController extends Controller {

    @Override
    public Navigation run() throws Exception {
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        AccountHelper.SetAuthCipherKey();
        Calendar calender = Calendar.getInstance(Locale.JAPAN);
        Date nowDate = new Date(calender.getTimeInMillis());
        int month = calender.get(Calendar.MONTH) + 1;
        ScheduleMeta meta = ScheduleMeta.get();
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm-dd-yyyy");
        List<Schedule> schedules =
            Datastore
                .query(meta)
                .filter(
                    meta.month.greaterThanOrEqual(month),
                    meta.month.lessThanOrEqual(month))
                .asList();
        List<Schedule> notifySchedules = new ArrayList<Schedule>();
        for (Schedule schedule : schedules) {
            Calendar cal = Calendar.getInstance(Locale.JAPAN);
            cal.setTime(dateFormat.parse(schedule.getDate()));
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) + 1);
            cal
                .set(
                    Calendar.MONTH,
                    cal.get(Calendar.MONTH) == 11
                        ? 0
                        : cal.get(Calendar.MONTH) - 1);
            Date date = new Date(cal.getTimeInMillis());
            float time = (date.getTime() - nowDate.getTime()) / 86400000f;
            if (time  <1&&time>=0) {
                notifySchedules.add(schedule);
            }
            response.getWriter().write(time+"\n\n");
        }
        if (notifySchedules.size() <1)
            return null;
        ProfileMeta profileMeta = ProfileMeta.get();
        List<Profile> activeMembersList =
            Datastore
                .query(profileMeta)
                .filter(profileMeta.isOB.equal(false))
                .asList();
        // ユーザーごとにメールの送信処理をする
        for (Profile profile : activeMembersList)
        {
            String content="";
            String scheduleMessage = "";
            for(Schedule schedule:notifySchedules)
            {
                boolean needSend=false;
                if(schedule.getRoleTag().isEmpty())
                {
                    needSend=true;
                }
                if(!needSend)
                {
                    for(RoleGlue glues:profile.getRoleRef().getModelList())
                    {
                        if(schedule.getRoleTag().indexOf(glues.getRoleRef().getModel().getRoleName())>-1)
                        {
                            needSend=true;
                            break;
                        }
                    }
                }
                if(needSend){
                    scheduleMessage+=("タイトル:["+schedule.getTitle()+"]<br>");
                    scheduleMessage+=("内容:<br>"+schedule.getContent().getValue()+"<br><br>");
                }
            }
            Calendar tomorrow=Calendar.getInstance(Locale.JAPAN);
            tomorrow.add(Calendar.DAY_OF_MONTH, 1);

            content=
                    "明日(今年の"+tomorrow.get(Calendar.DAY_OF_YEAR)+"日目)予定されているスケジュールをお伝えします。<br><br>"
                        + scheduleMessage
                        + "です。<br><br>"
                        + "尚、このメールは配信専用です。";
            Queue queue=QueueFactory.getQueue("mail-queue");
            TaskOptions to = TaskOptions.Builder.withUrl("/queue/mail").param("to",  profile.getAuthorizationInverseModelRef().getModel().getId())
                    .param("title",URLEncoder.encode("【OSK Kernel.net】明日の予定の連絡【自動送信】","utf-8") ).param("content", URLEncoder.encode(content,"utf-8"));
            queue.add(to.method(Method.POST));
            response.getWriter().write(profile.getAuthorizationInverseModelRef().getModel().getId()+"\n\n\n");
            response.getWriter().write(content+"\n\n\n\n\n\n");
        }
        return null;
    }
}
