package osk.web.controller.cron.day;

import org.slim3.tester.ControllerTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ScheduleControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.start("/cron/day/schedule");
        ScheduleController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is(nullValue()));
    }
}
