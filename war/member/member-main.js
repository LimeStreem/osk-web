﻿
$(function () {
    goHome();
    $.postJSON("/member/meta", null, function (json) {
        $("#account-management").html(json.user);
        $("#account-icon").attr("src", json.icon);
        if (json.isAdmin == "true") {
            $("#account-dropdown").append("<li class=\"dropdown-header\">管理者権限</li>" +
                "<li><a href=\"#register\" onclick=\"goRegisterPage()\">アカウントの追加</a></li>"
            + "<li><a href=\"#delete\" onclick=\"goDeletePage()\">アカウントの削除</a></li>" +
                "<li><a href=\"#schedule\" onclick=\"goSchedulePage()\">スケジュール編集</a></li>"
            +"<li><a href=\"#tag\" onclick=\"goTagPage()\">タグ編集</a></li>");
        }
    });
});


function goRegisterPage() {
    $("#member-loading-progress").show();
    $("#content").load("admin/register.html",null,function() {
        $("#member-loading-progress").hide();
    });
}

function goHome() {
    $("#member-loading-progress").show();
    $("#content").load("top.html", null, function () {
        $("#member-loading-progress").hide();
    });
    $("#side-menu li").removeClass("active");
    $("#side-menu-info").addClass("active");
}

function goLibrary() {
    $("#member-loading-progress").show();
    $("#content").load("library.html",null, function() {
        $("#member-loading-progress").hide();
    });
    $("#side-menu li").removeClass("active");
    $("#side-menu-library").addClass("active");
}

function goMail() {
    $("#member-loading-progress").show();
    $("#content").load("mail.html",null,function() {
        $("#member-loading-progress").hide();
    });
    $("#side-menu li").removeClass("active");
    $("#side-menu-mail").addClass("active");
}

function goMemberList() {
    $("#member-loading-progress").show();
    $("#content").load("memberList.html",null, function() {
        $("#member-loading-progress").hide();
    });
    $("#side-menu li").removeClass("active");
    $("#side-menu-memberList").addClass("active");
}

function goSchedulePage()
{
    $("#member-loading-progress").show();
    $("#content").load("admin/schedule.html", null, function () {
        $("#member-loading-progress").hide();
    });
}

function goTagPage() {
    $("#member-loading-progress").show();
    $("#content").load("admin/tag.html", null, function () {
        $("#member-loading-progress").hide();
    });
}

function Register() {
    //フォームの内容を取得し

    var id = $("#form-id").val();
    var pass = $("#form-pass").val();
    if (id == "" || pass == "")
    {
        $("#login-form-result").text("入力されていません。");
        return;
    }
    $("#form-register-button").button('loading');
    $.postJSON("/auth/register", { "id": id, "pass": pass }, function (json)
    {
        //結果をJSONで取得
        if (json.status == "duplicated") {
            $("#login-form-result").text("そのメールアドレスはすでに使用されています。");
            $("#form-register-button").button('reset');
        } else if (json.status == "success")
        {
            $("#login-form-result").text("登録しました");
            $("#form-register-button").button('reset');
        }
    });
}

function goDeletePage() {
    $("#content").load("admin/delete.html", null, function () {
        $.postJSON("/auth/users", null, function (json) {
            targetUserNames = new Array(json.count);
            for (var i = 0; i < json.count; i++) {
                targetUserNames[i] = json.ids[i];
                $("#account-list").append("<a class=\"list-group-item\" onclick=\"ActivateDeleteItem(" + i + ")\">" + json.ids[i] + "\"" + json.names[i] + "\"" + "</a>");
            }
        });
    });
}

function goProfileEdit()
{
    $("#content").load("profile.html", null, function () {
            $.postJSON("/profile/", function (json) {
        $("#form-name").val(decodeURIComponent(json.name));
        $("#form-nickName").val(decodeURIComponent(json.nickName));
        $("#form-mailAddress").val(decodeURIComponent(json.mailAddress));
        $("#form-schoolNumber").val(decodeURIComponent(json.schoolNumber));
        AdjustFaculty();
        $("#form-department").val($("#form-department option:contains('"+decodeURIComponent(json.department)+"')").val());
        $("#form-faculty").val(decodeURIComponent(json.major));
        $("#form-zipCode").val(json.zipCode);
        $("#form-address").val(decodeURIComponent(json.address));
        $("#form-githubURL").val(decodeURIComponent(json.githubURL));
        $("#form-bitBucketURL").val(decodeURIComponent(json.bitBucketURL));
        $("#form-twitterUserName").val(decodeURIComponent(json.twitterUserName));
        $("#form-url").val(decodeURIComponent(json.url));
        $("#form-usableLanguage").val(decodeURIComponent(json.usableLanguage));
        $("#form-favoriteAnime").val(decodeURIComponent(json.favoriteAnime));
        $("#form-favoriteFormula").val(decodeURIComponent(json.favoriteFormula));
        $("#form-selfIntroduction").val(decodeURIComponent(json.selfIntroduction));
        if (json.isOB) {
            $("#form-isOB").val("true");
        }
        else {
            $("#form-isOB").val("false");
        }
        if (json.isMale) {
            $("#form-sex").val("true");
        }
        else {
            $("#form-sex").val("false");
        }
    });
    });

}

function EditProfile() {
    var name = $("#form-name").val();
    var nickName = $("#form-nickName").val();
    var mailAddress = $("#form-mailAddress").val();
    var schoolNumber = $("#form-schoolNumber").val();
    var zipCode = $("#form-zipCode").val();
    var address = $("#form-address").val();
    var githubURL = $("#form-githubURL").val();
    var bitBucketURL = $("#form-bitBucketURL").val();
    var twitterUserName = $("#form-twitterUserName").val();
    var url = $("#form-url").val();
    var usableLanguage = $("#form-usableLanguage").val();
    var favoriteAnime = $("#form-favoriteAnime").val();
    var favoriteFormula = $("#form-favoriteFormula").val();
    var selfIntroduction = $("#form-selfIntroduction").val();
    var isOB = $('#form-isOB option:selected').val();
    var sex = $('#form-sex option:selected').val();
    var department = $("#form-department option:selected").text();
    var major = $("#form-faculty").val();
    $.postJSON("/profile/edit", { "name": encodeURIComponent(name), "nickName": encodeURIComponent(nickName), "mailAddress": encodeURIComponent(mailAddress), "schoolNumber": encodeURIComponent(schoolNumber), "zipCode": encodeURIComponent(zipCode), "address":encodeURIComponent(address), "githubURL": encodeURIComponent(githubURL), "bitBucketURL": encodeURIComponent(bitBucketURL), "twitterUserName": encodeURIComponent(twitterUserName), "url": encodeURIComponent(url), "usableLanguage": encodeURIComponent(usableLanguage), "favoriteAnime": encodeURIComponent(favoriteAnime), "favoriteFormula": encodeURIComponent(favoriteFormula), "selfIntroduction": encodeURIComponent(selfIntroduction), "isOB": isOB, "isMale": sex ,"department":encodeURIComponent(department),"major":encodeURIComponent(major)}, function (json) {
        if (json.status == "success") {
            $("#login-form-result").addClass("alert alert-success");
            $("#login-form-result").text("プロフィールを更新しました。");
        }
    });
}


function ActivateDeleteItem(index) {
    $("#account-list a").removeClass("active");
    $("#account-list a:nth-child(" + (index + 2) + ")").addClass("active");
    $("#target-id").html(targetUserNames[index]);

}

function DeleteTarget() {
    $("#form-delete-button").button('loading');
    if(window.confirm("後悔はないか?"))
        $.postJSON("/auth/delete", { "id": $("#target-id").text() }, function (json) {
            if (json.status == "error:isAdmin") {
                alert("管理者権限が付与されているアカウントは消すことができない。管理コンソールより削除すること。");
                $("#form-delete-button").button('reset');
                return;
            }
            $("#delete-result").text("完了");
        goHome();
    });
}

function AdjustFaculty() {
    $("#form-faculty").empty();
    switch ($("#form-department").val()) {
        case "day-science":
            $("#form-faculty").append("<option>数学科</option><option>物理学科</option><option>化学科</option><option>数理情報科学科</option><option>応用物理学科</option><option>応用化学科</option>");
            break;
        case "day-engineering":
            $("#form-faculty").append("<option>建築学科</option><option>工業化学科</option><option>電気工学科</option><option>経営工学科</option><option>機械工学科</option>");
            break;
        case "pharmacy":
            $("#form-faculty").append("<option>薬学科</option><option>生命創薬科学科</option>");
            break;
        case "science-and-engineering":
            $("#form-faculty").append("<option>数学科</option><option>物理学科</option><option>情報科学科</option><option>応用生物科学科</option><option>建築学科</option><option>工業化学科</option><option>電気電子情報工学科</option><option>経営工学科</option><option>機械工学科</option><option>土木工学科</option>");
            break;
        case "fundamental-engineering":
            $("#form-faculty").append("<option>電子応用工学科</option><option>材料工学</option><option>生物工学科</option>");
            break;
        case "business-administration":
            $("#form-faculty").append("<option>経営学科</option>");
            break;
        case "night-science":
            $("#form-faculty").append("<option>数学科</option><option>物理学科</option><option>化学科</option>");
            break;
        case "night-engineering":
            $("#form-faculty").append("<option>建築学科</option><option>電気工学科</option><option>経営工学科</option>");
            break;
        default:
            $("#form-faculty").append("<option>その他</option>");
            break;
    }
}