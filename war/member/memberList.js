﻿function getClassByType(type) {
    if (type == "重役") {
        return "label label-danger";
    } else if (type == "役職") {
        return "label label-success";
    } else if (type == "タグ") {
        return "label label-primary";
    } else {
        return "label label-info";
    }
}

function OpenProfile(id) {
    $("#profileModal").modal("show");
    $("#profile-loading-progress").show();
    $.postJSON("/profile/get", { "id": String(id) }, function (json) {
        $("#profile-roles").html("");
        $("#profile-img").attr("src", json.icon);
        $("#profile-name").text(decodeURIComponent(json.name));
        if (json.nick != "")
            $("#profile-nickname").text("(" + decodeURIComponent(json.nick) + ")");
        else
            $("#profile-nickname").text("");
        $("#profile-major").text(decodeURIComponent(json.department) + decodeURIComponent(json.major));
        $("#profile-usable-language").html(decodeURIComponent(json.usableLanguage));
        $("#profile-favorite-anime").html(decodeURIComponent(json.favoriteAnime));
        $("#profile-self-introduce").html(decodeURIComponent(json.selfIntroduction));
        $("#profile-favorite-formula").attr("src", "http://chart.apis.google.com/chart?cht=tx&chf=bg,s,ffffff00&chco=000000ff&chl=" + json.favoriteFormula);
        for (var i = 0; i < json.count; i++) {
            $("#profile-roles").append($('<p class="'+decodeURIComponent(getClassByType(json.types[i]))+'">'+decodeURIComponent(json.roles[i])+'</p>'))
        }
        var url = decodeURIComponent(json.url);
        if (url== null || url == ""||url=="undefined") {
            $("#profile-url").addClass("disabled");
        } else {
            $("#profile-url").removeClass("disabled");
            $("#profile-url").attr("href", url);
        }
        var twitterUser = decodeURIComponent(json.twitter);
        if (twitterUser == null||twitterUser==""||twitterUser=="undefined") {
            $("#profile-twitter").addClass("disabled");
        } else {
            $("#profile-twitter").removeClass("disabled");
            $("#profile-twitter").attr("href", "https://twitter.com/" + twitterUser);
        }
        var gitHubURL = decodeURIComponent(json.github);
        if (gitHubURL == null || gitHubURL == ""||gitHubURL=="undefined") {
            $("#profile-github").addClass("disabled");
        } else {
            $("#profile-github").removeClass("disabled");
            $("#profile-github").attr("href",gitHubURL);
        }
        var bitbucketURL = decodeURIComponent(json.bitbucket);
        if (bitbucketURL == null || bitbucketURL == ""||bitbucketURL=="undefined") {
            $("#profile-bitbucket").addClass("disabled");
        } else {
            $("#profile-bitbucket").removeClass("disabled");
            $("#profile-bitbucket").attr("href",bitbucketURL)
        }
        if (json.isOB) {
            $("#profile-isob").text("OB/OG");
        } else {
            $("#profile-isob").text("現役");
        }
        $("#profile-loading-progress").hide();
    });

}
$(function () {
    
    goActiveMember();
    
}
);

function ReloadList(val) {
    $("#memberlist-loading-progress").show();
    $("#member-list-control").html("");
    $.postJSON("/member/list/", val, function (json) {
        for (var i = 0; i < json.count; i++) {
            var roleTags = "";
            for (var j = 0; j < json.counts[i]; j++) {
                var roleType = decodeURIComponent(json.types[i][j]);
                roleTags += '<p class="' + getClassByType(roleType) + ' member-list-role">' + json.roles[i][j] + '</p>';
            }
            $("#member-list-control").append($("<tr><td width=\"115\"><img src=\"" + json.icons[i] + "\"></img></td><td width=\"160\">" + decodeURIComponent(json.names[i]) + "</td><td width=\"160\">" + decodeURIComponent(json.department[i]) + "</td><td>" + decodeURIComponent(json.major[i]) + '</td><td>' + roleTags + '</td><td><a class="btn btn-primary glyphicon glyphicon-user" onclick="OpenProfile(' + json.ids[i] + ');" >    </a></td></tr>'));
        }
        $("#memberlist-loading-progress").hide();
    }
    );
}

function goActiveMember() {
    ReloadList({"type":0});
    selectTab("#active-member");
    $("#role-list-table").hide();
    $("#member-list-table").show();
}

function goOBMember() {
    ReloadList({ "type": 1 });
    selectTab("#ob-member");
    $("#role-list-table").hide();
    $("#member-list-table").show();
}

function  goAllMember() {
    ReloadList({ "type": 2 });
    selectTab("#all-member");
    $("#role-list-table").hide();
    $("#member-list-table").show();
}

function goRoleList() {
    ReloadRoleList();
    selectTab("#role-list");
    $("#member-list-table").hide();
    $("#role-list-table").show();
}


function selectTab(tabname) {
    $("#member-list-tab li").removeClass("active");
    $(tabname).addClass("active");
}

function ReloadRoleList() {
    $("#memberlist-loading-progress").show();
    $("#tag-list-control").html("");
    $.postJSON("/role/list", null, function (json) {
        tags = new Array(json.count);
        tagNames = new Array(json.count);
        tagCount = json.count;
        for (var i = 0; i < json.count; i++) {
            var nameTags = "";
            for (var j = 0; j < json.namesCounts[i]; j++) {
                nameTags += '<p class="label label-success">' + decodeURIComponent(json.appliedNames[i][j]) + '</a>';
            }
            $("#tag-list-control").append('<tr><td><p class="' + decodeURIComponent(getClassByType(json.type[i])) + '">' + decodeURIComponent(json.names[i]) + '</p></td><td>' + nameTags + '</td></tr>');
            tags[i] = json.ids[i];
            tagNames[i] = decodeURIComponent(json.names[i]);
        }
        $("#memberlist-loading-progress").hide();
    });
}