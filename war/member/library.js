﻿$(function () {
    goLibraryClubTab();
    
});
function goLibraryClubTab() {
    $("#library-tab li").removeClass("active");
    $("#library-tab-all").addClass("active");
    CurrentLibrarytab = "全体";
    RefleshLibraryList();
}

function goLibraryCTab()
{
    $("#library-tab li").removeClass("active");
    $("#library-tab-c").addClass("active");
    CurrentLibrarytab = "C班";
    RefleshLibraryList();
}

function goLibraryJavaTab()
{
    $("#library-tab li").removeClass("active");
    $("#library-tab-java").addClass("active");
    CurrentLibrarytab = "Java班";
    RefleshLibraryList();
}

function goLibraryScriptTab()
{
    $("#library-tab li").removeClass("active");
    $("#library-tab-script").addClass("active");
    CurrentLibrarytab = "Script班";
    RefleshLibraryList();
}

function goLibraryGeneralMeetingTab()
{
    $("#library-tab li").removeClass("active");
    $("#library-tab-general-meeting").addClass("active");
    CurrentLibrarytab = "総会";
    RefleshLibraryList();
}

function RefleshLibraryList() {
    $("#library-loading-progress").show();
    $.postJSON("/library/list", { "category": CurrentLibrarytab }, function (json) {
        blobKeys = new Array(json.count);
        fileNames = new Array(json.count);
        fileDescs = new Array(json.count);
        $("#library-list-box").html("");
        if (json.count == 0) {
            $("#library-list-box").append("<div class=\"bs-callout bs-callout-warning\"><h4>このカテゴリに現在要素は存在していません!</h4>要素を追加する際は右上の雲のマークのボタンから行ってください。</div>");
        }
        for (var i = 0; i < json.count; i++) {
            blobKeys[i] = json.blobKeys[i];
            fileNames[i] = decodeURIComponent(json.titles[i]);
            fileDescs[i] = decodeURIComponent(json.descs[i]);
            $("#library-list-box").append("<div class=\"bs-callout bs-callout-info\"><h4>" +decodeURIComponent(json.titles[i]) + "<p class=\"text-right\"><small>最終更新日時:" + json.date[i] + "  /  アップロード者:" + decodeURIComponent(json.auther[i]) + "</small></p></h4>" + "<p class=\"text-right \"><strong class=\"" +( json.sizes[i] > 100000000 ? "text-danger":(json.sizes[i]>50000000?"text-warning":""))+"\">サイズ:"+json.sizes[i]+"bytes</strong>  / <small> MD5 Hash:"+json.hashes[i]+"</small></p></h4>"+
                "<p>" + decodeURIComponent(json.descs[i]) + "</p><div class=\"row\"><div class=\"col-md-8\"></div>"+(json.hasRight[i]=="true"?"":"<div class=\"col-md-2\"></div>")+"<a class=\"btn btn-primary col-md-1 btn-success glyphicon glyphicon-arrow-down\" style=\"margin-left:5px;\" onclick=\"GetFile(" + i + ")\">  </a>" +(json.hasRight[i]=="true"?
                "<a class=\"btn btn-primary col-md-1 btn-info glyphicon glyphicon-pencil\" onclick=\"EditFile(" + i + ")\"  style=\"margin-left:5px;\" >  </a>" +
                "<a class=\"btn btn-primary col-md-1 btn-danger  glyphicon glyphicon-remove-sign\" onclick=\"DeleteFile(" + i + ")\"  style=\"margin-left:5px;\" >  </a>" :"" )+
                "<div class=\"col-md-1\"></div></div></div></div>");
        }
        $("#library-loading-progress").hide();
    });
}

function GetFile(key) {
    location.href="/library/download?blobKey=" + blobKeys[key];
}

function DeleteFile(key)
{
    if (confirm("もとには戻せません。消してよろしいですか?")) {
        $.postJSON("/library/delete", { "blobKey": blobKeys[key] }, function() {
            alert("削除完了!");
            ReloadCurrentPage();
        });
    }
}

function EditFile(key) {
    $("#editModal").modal("show");
    $("#edit-form-title").val(fileNames[key]);
    $("#edit-form-desc").val(fileDescs[key]);
    $("#edit-form-send").attr("onclick", "EditComplete(" + key + ");");
}

function EditComplete(key) {
    $.postJSON("/library/edit", { "blobKey": blobKeys[key], "title": $("#edit-form-title").val(), "desc": $("#edit-form-desc").val() }
    ,function(json) {
        if (json.status == "success") {
            RefleshLibraryList();
            $("#editModal").modal("hide");
        }
    });
    
}

function ShowUpload()
{
    $("#upload-modal-header").text("\"" + CurrentLibrarytab + "\"に資料を添付");
    
}

Dropzone.options.myDropzone = {
    maxFilesize: 2048,
    init: function () {
        this.on("uploadprogress", function (file, progress) {
            console.log("File progress", progress);
        });
    }
}

function goUpload() {
    $.postJSON("/library/add", { "title": $("#upload-form-title").val(), "desc": $("#upload-form-desc").val(), "category": CurrentLibrarytab }, function (json) {
        CurrentProcessingId = json.id;
        $(".dropzone").dropzone({
            maxFilesize: 2048,
            url: json.url,
            success: function() {
                ReloadCurrentPage();
                $("#uploadModal").modal("hide");
                $("#descModal").modal("hide");
                window.location.reload();

            },
            canceled:function() {
                $.postJSON("/library/cancel", { "id": CurrentProcessingId }, function () {
                });
                $("#uploadModal").modal("hide");
                $("#descModal").modal("hide");
                window.location.reload();
            }
            }
        );
    });
}

function ReloadCurrentPage()
{
    if (CurrentLibrarytab == "全体") {
        goLibraryClubTab();
    }else if (CurrentLibrarytab == "C班") {
        goLibraryCTab();
    }else if (CurrentLibrarytab == "Java班") {
        goLibraryJavaTab();
    }else if (CurrentLibrarytab == "Script班") {
        goLibraryScriptTab();
    }else if (CurrentLibrarytab == "総会") {
        goLibraryGeneralMeetingTab();
    }
}

function UploadCancel() {
    $.postJSON("/library/cancel", { "id": CurrentProcessingId }, function (json) {
    });
    $("#uploadModal").modal("hide");
    $("#descModal").modal("hide");
}