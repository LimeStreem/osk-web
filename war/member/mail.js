﻿$(function () {
    targetSet = new Set();
    $.postJSON("/role/list", null, function (json) {
        $("#tag-list-control").empty();
        tags = new Array(json.count);
        tagNames = new Array(json.count);
        tagCount = json.count;
        for (var i = 0; i < json.count; i++) {
            var nameTags = "";
            for (var j = 0; j < json.namesCounts[i]; j++) {
                nameTags += '<p class="label label-success">' + decodeURIComponent(json.appliedNames[i][j]) + '</p>';
            }
            $("#tag-list-control").append('<tr><td><a class="' + decodeURIComponent(getClassByType(json.type[i])) + '" onclick="AddSendTarget('+i+')">' + decodeURIComponent(json.names[i]) + '</a></td><td>' + nameTags + '</td></tr>');
            tags[i] = json.ids[i];
            tagNames[i] = decodeURIComponent(json.names[i]);
        }

    });
});

function AddSendTarget(index) {
    $("#mail-target").val($("#mail-target").val() + tagNames[index] + " ");
}

function AddTargetByName(name) {
    $("#mail-target").val($("#mail-target").val() + name + " ");
}

function getClassByType(type) {
    if (type == "重役") {
        return "label label-danger";
    } else if (type == "役職") {
        return "label label-success";
    } else if (type == "タグ") {
        return "label label-primary";
    } else {
        return "label label-info";
    }
}

function SendMail() {
    var content = $("#mail-content").val();
    content = content.replace(/\r\n/g, "<br />");
    content = content.replace(/(\n|\r)/g, "<br />");
    var title = $("#mail-title").val();
    var target = $("#mail-target").val();
    $.postJSON("/mail/send", { "content": encodeURIComponent(content), "title": encodeURIComponent(title), "target": encodeURIComponent(target) }, function(json) {
        if (json.status == "success") {
            $("#mail-content").val("");
            $("#mail-target").val("");
            $("#mail-title").val("");
            alert("メール送信成功");
        }
    });
}