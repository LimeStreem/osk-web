﻿caldata = {};
$(function () {
    $("#top-loading-progress").show();
    cal=$("#calendar").calendario({ displayMonthAbbr: true, displayWeekAbbr: true
    });
    $.postJSON("/calender/get", { "month": cal.getMonth() },function(json) {
        for (var i = 0; i < json.count; i++) {
            var strClass = "label ";
            var tag = decodeURIComponent(json.tags[i]);
            if (tag == "活動") {
                strClass += "label-primary";
            }else if (tag == "ゼミ") {
                strClass += "label-info";
            }else if (tag == "会議") {
                strClass += "label-warning";
            }else if (tag == "総会") {
                strClass += "label-success";
            }else if (tag== "提出") {
                strClass += "label-danger";
            }
            caldata[json.dates[i]] = '<p class="'+strClass+'">' + tag + '</p>';

        }
        cal.setData(caldata);
        $("#top-loading-progress").hide();
    });
    $month = $('#custom-month').html(cal.getMonthName()),
    $year = $('#custom-year').html(cal.getYear());
    $('#custom-next').on('click', function () {
        cal.gotoNextMonth(updateMonthYear);
    });
    $('#custom-prev').on('click', function () {
        cal.gotoPreviousMonth(updateMonthYear);
    });
});


function updateMonthYear() {
    $month.html(cal.getMonthName());
    $year.html(cal.getYear());
}

