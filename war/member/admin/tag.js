﻿function getClassByType(type) {
    if (type == "重役") {
        return "label label-danger";
    }else if (type == "役職") {
        return "label label-success";
    }else if (type == "タグ") {
        return "label label-primary";
    } else {
        return "label label-info";
    }
}

$(function () {
    $("#tag-loading-progress").show();
    ReloadRoleList();
});

function addRole() {
    $.postJSON("/role/add", {"name":encodeURIComponent($("#tag-add-name").val()),"type":encodeURIComponent($("#tag-add-type").val())}, function(json) {
        if (json.status == "success") {
            alert("追加完了.");
        }
        ReloadRoleList();
    });
}

function ReloadRoleList() {
    $("#tag-list-control").html("");
    $("#tag-apply-select").html("");
    $(".tag-apply-selecttag").html("");
    $.postJSON("/role/list", null, function(json) {
        tags = new Array(json.count);
        tagNames = new Array(json.count);
        tagCount = json.count;
        for (var i = 0; i < json.count; i++) {
            var nameTags = "";
            for (var j = 0; j < json.namesCounts[i]; j++) {
                nameTags += '<p class="label label-success">' + decodeURIComponent(json.appliedNames[i][j]) + '</a>';
            }
            $("#tag-list-control").append('<tr><td><p class="'+decodeURIComponent(getClassByType(json.type[i]))+'">' + decodeURIComponent(json.names[i]) + '</p></td><td>'+nameTags+'</td></tr>');
            tags[i] = json.ids[i];
            tagNames[i] = decodeURIComponent(json.names[i]);
        }
        $.postJSON("/member/list/", null, function(json2) {
            for(var j=0;j<json2.count;j++){
                $("#tag-apply-select").append($('<option value="'+json2.ids[j]+'">'+json2.ids[j]+":("+decodeURIComponent(json2.names[j])+")"+'</option>'));
            }
            for (var j = 0; j < tagCount; j++) {
                $(".tag-apply-selecttag").append($('<option value="' + tags[j] + '"><a class="label label-primary">'+tagNames[j]+'</a></option>'));
            }
            $("#tag-loading-progress").hide();
        });

    });
}

function ApplyTag() {
    $.postJSON("/role/apply", { "id": $("#tag-apply-select").val(), "tag": $("#tag-apply-selecttag").val() }, function() {
        alert("追加完了.");
        ReloadRoleList();
    });
}

function deleteRole() {
    $.postJSON("/role/delete", {"id": $("#tag-delete-select").val() }, function () {
        alert("削除完了.");
        ReloadRoleList();
    });
}
