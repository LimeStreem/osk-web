﻿function AddSchedule() {
    var content = $("#schedule-desc").val();
    content = content.replace(/\r\n/g, "<br />");
    content = content.replace(/(\n|\r)/g, "<br />");
    $.postJSON("/calender/add", {"title":encodeURIComponent($("#schedule-title").val()),"desc":encodeURIComponent(content),"tag":encodeURIComponent($("#schedule-tag").val()),"role":encodeURIComponent($("#schedule-role").val()),"date":encodeURIComponent($("#datepicker").val())}, function () {
        alert("追加完了!");
    });
}

$(function () {
    $("#datepicker").datepicker();
});