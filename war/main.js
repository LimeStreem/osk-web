﻿jQuery(function ($) {
    defineAnchorAnimation();
    konamicommand(function () {
        var splited = window.location.href.split('#');
        window.location.href = window.location.href + "#login";
        goLogin();
    });
});

defineAnchorAnimation = function () {
    $('a[href^="#"].scroll').click(function () {
        if (!isHome) goHome(animate(this));
        else animate(this);
        return false;
    });
};
animate = function(param) {
    $('html, body').animate(
    { scrollTop: $($(param).attr('href')).offset().top }
);
};

goClubRoom = function () {
    $("#content").load("clubroom.htm");
    navbarSelect("#navbar-clubroom");
    /* This is basic - uses default settings */

    $("a.gallary").fancybox();
    isHome = false;
};

goHome = function () {
    $("#content").load("home.html");
    navbarSelect("#navbar-home");

    isHome = true;
};

goContact = function () {
    $("#content").load("contact.html");
    navbarSelect("#navbar-contact");
    isHome = true;
};

goHome = function (func) {
    $("#content").load("home.html", null, func);
    navbarSelect("#navbar-home");
    isHome = true;
};

goAbout = function () {
    $("#content").load("about.html");
    navbarSelect("#navbar-about");
    isHome = false;
};

goLogin = function() {
    $("#content").load("login.html");
    navbarSelect("#navbar-login");
    isHome = false;
};

navbarSelect = function(target) {
    $("#navbar-home").removeClass("active");
    $("#navbar-group").removeClass("active");
    $("#navbar-clubroom").removeClass("active");
    $("#navbar-contact").removeClass("active");
    $("#navbar-about").removeClass("active");
    $(target).addClass("active");
};

function konamicommand(func, cmd) {
    if (!(func instanceof Function)) return;
    var kc = {};
    kc.func = func;
    kc.cmd = (typeof (cmd) == "string" || cmd instanceof String) ? cmd : '|38|38|40|40|37|39|37|39|66|65|';
    kc.count = 0;
    kc.input = '|';
    kc.up = function (event) {
        event = event ? event : window.event;
        if (kc.cmd.indexOf('|' + event.keyCode + '|') == -1) {
            kc.input = '|';
            return;
        }
        kc.input += event.keyCode + '|';
        if (kc.input.indexOf(kc.cmd) != -1) {
            kc.func(kc.count);
            kc.count++;
            kc.input = '|';
        }
    }
    if (document.attachEvent) {
        document.attachEvent('onkeyup', kc.up);
    } else if (document.addEventListener) {
        document.addEventListener('keyup', kc.up, false);
    }
}

function Login() {//ログイン時に呼び出される
    //フォームの内容を取得し
    var id = $("#form-id").val();
    var pass = $("#form-pass").val();
    $("#form-submit").button('loading');
    $("#login-progress").show();
    $.postJSON("auth/login", { "id": id, "pass": pass }, function (json) {
        //結果をJSONで取得
        if (json.status == "failed") {
            $("#login-form-result").text("IDもしくはパスワードが違います。");
            $("#form-submit").button('reset');
            $("#login-progress").hide();
        } else if (json.status == "success") {
            //ログインできるならそのままメンバーページへ
            location.href = "/member/";
        }
});
}
